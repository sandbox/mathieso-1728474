<?php
/**
 * @file
 * Users can be given extra roles for domains listed in domain reference fields.
 */

module_load_include('inc', 'domain_reference_roles', 'domain_reference_roles.blocks');


/**
 * Implements hook_help().
 */
function domain_reference_roles_help($path, $arg) {
  switch ($path) {
    case 'admin/help#domain_reference_roles':
      $output = '<h3>' . t('About') . '</h3>';
      $output .= '<p>' . t('The Domain Reference Roles module lets you give individual users ' .
          'diffrent roles on different domains. It works in conjuction with the ') . 
          l('Domain Access ', 'http://drupal.org/project/domain') . 
          t(' and Domain Reference modules.') . '</p>
          <p>' . t('Create one or more Domain Reference fields on users. Assign each field ' .
          'to a role.') . '</p>
        ';
      return $output;
      break;
  }
}

/**
 * Implements hook_menu().
 */
function domain_reference_roles_menu() {
  $items['admin/structure/domain/domain-roles'] = array(
    'title' => 'Domain reference roles',
    'description' => 'Give users roles, depending on the domain.',
    'access arguments' => array('administer domains'),
    'type' => MENU_LOCAL_TASK,
    'page callback' => 'drupal_get_form',
    'page arguments' => array('_dr_field_role_form'),
    'file' => 'domain_reference_roles.admin.inc',
    'weight' => 0,
  );
  $items['admin/structure/domain/domain-roles/mapping'] = array(
    'title' => 'Fields adding roles',
    'type' => MENU_DEFAULT_LOCAL_TASK,
  );
  $items['admin/structure/domain/domain-roles/delete/%/%'] = array(
    'title' => 'Domain reference roles',
    'access arguments' => array('administer domains'),
    'page callback' => 'drupal_get_form',
    'page arguments' => array('domain_reference_roles_confirm_delete_form', 5, 6),
    'file' => 'domain_reference_roles.admin.inc',
    'type' => MENU_CALLBACK,
  );
//  $items['t1'] = array(
//    'title' => t('Thing'),
//    'type' => MENU_NORMAL_ITEM,
//    'access callback' => TRUE,
//    'page callback' => 't1',
//  );
//  $items['t2'] = array(
//    'title' => t('Instances'),
//    'type' => MENU_NORMAL_ITEM,
//    'access callback' => TRUE,
//    'page callback' => 't2',
//  );
  return $items;
}

//function t1() {
//  $i = field_info_instances();
//  dpr("\n\n\n\n\nInstances");
//  kpr($i);
//  $t = field_info_field_types();
//  dpr('Field types');
//  kpr($t);
//  $f = field_info_fields();
//  dpr('Fields');
//  kpr($f);  $b = field_info_bundles();
//  dpr('Bundles');
//  kpr($b);
//  $e = entity_get_info();
//  dpr('Entities');
//  kpr($e);
//  return 'DOG';
//}
//
//function t2(){
//  $all_instances = field_info_instances();
//  $all_fields = field_info_fields();
//  $all_entities = entity_get_info();
//  $all_bundles = field_info_bundles();
//  $er_instances = array();
//  foreach ( $all_instances as $entity_name => $bundles ) {
//    foreach ( $bundles as $bundle_name => $fields ) {
//      foreach ( $fields as $field_name => $field ) {
//        $field_type = $all_fields[$field_name]['type'];
//        if ( $field_type == 'domain_reference' ) {
//          $er_instance = array();
//          $er_instance['entity_name'] = $entity_name;
//          $er_instance['entity_label'] = $all_entities[$entity_name]['label'];
//          $er_instance['entity_description'] = $all_entities[$entity_name]['description'];
//          $er_instance['bundle_name'] = $bundle_name;
//          $er_instance['bundle_label'] = $all_bundles[$entity_name][$bundle_name]['label'];
//          $er_instance['field'] = $field_name;
//          $er_instance['instance_id'] = $field['id'];
//          $er_instance['instance_label'] = $field['label'];
//          $er_instance['instance_description'] = $field['description'];
//          $er_instances[] = $er_instance;
//        } //End this is a domain reference instance.
//      } //End for each field in bundle (i.e., an instance).
//    } //End foreach bundle
//  } //End foreach instance
//  dpr('domain_reference instances');
//  dpr($er_instances);
//  return('Even more dogs');
//}

/**
 * Implements hook_theme().
 */
//function domain_reference_roles_theme($existing, $type, $theme, $path) {
//  $themes = array(
//    '_dr_field_role_form' => array(
//      'render element' => 'form',
//      'file' => 'domain_reference_roles.admin.inc',
//    ),
//  );
//  return $themes;
//}

/**
 * Implements hook_node_access.
 * @param type $node
 * @param type $op
 * @param type $account
 * @return string
 */
function domain_reference_roles_node_access($node, $op, $account) {
  //Compute the permission being sought. $node is content type for creating op.
  $type = is_string($node) ? $node : $node->type;
  $perm_sought = "$op $type content on assigned domains";
  return domain_reference_roles_check_permission($perm_sought);
}

/**
 * Check whether the current user has a permission.
 * $param stdClass $account Account to check, NULL for logged in user.
 * @param string $permission Permission to check.
 * @return boolean True if user has the permission.
 */
function domain_reference_roles_check_permission($permission, $account = NULL) {
  try {
    //Get the user data.
    if ( is_null($account) ) {
      global $user;
      $account = $user;
    }
    //Superuser can do anything.
    if ( $account->uid == 1 ) {
      return NODE_ACCESS_ALLOW;
    }
    //Anonymous check, just in case.
    if ( $account->uid == 0 ) {
      return NODE_ACCESS_IGNORE;
    }    
    //Try the cache.
    $perms_cache = &drupal_static(__FUNCTION__);
    if ( isset($perms_cache[$account->uid][$permission]) ) {
      return $perms_cache[$account->uid][$permission];
    }
    $result = NULL;
    //Get the mappings from domain reference field to role.
    $mappings = new FieldRoleMappingCollection();
    if ( $mappings->countMappings() == 0 ) {
      //No field-role mappings.
      $result = NODE_ACCESS_IGNORE;
    }
    else {
      //Get the extra roles this user has.
      $extra_roles = domain_reference_roles_get_extra_roles($account, $mappings);
      //Get the permissions for the extra roles.
      $extra_role_perms = user_role_permissions($extra_roles);
      //Check whether permission is in role perms array.
      foreach ( $extra_role_perms as $role_perms) {
        if ( array_key_exists($permission, $role_perms) !== FALSE ) {
          $result = NODE_ACCESS_ALLOW;
          break;
        }
      }
      if (is_null($result) ) {
        $result = NODE_ACCESS_IGNORE;
      }
    } //End there are mappings.
    //Store in cache
    $perms_cache[$account->uid][$permission] = $result;
    return $result;
  } catch (Exception $e) {
    domain_reference_roles_log_exception('Domain reference roles', $e);
  }
}

/**
 * Compute the extra roles a user gets on the curent domain.
 * @param type $account User.
 * @param FieldRoleMappingCollection $mappings Field-role mappings.
 *   Passed just for efficiency.
 * @return array Extra roles. id => name.
 */
function domain_reference_roles_get_extra_roles(
    $account, 
    FieldRoleMappingCollection $mappings = NULL){
  try {
    //Try the cache first.
    $extra_roles_cache = &drupal_static(__FUNCTION__);
    if ( isset($extra_roles_cache[$account->uid]) ) {
      return $extra_roles_cache[$account->uid];
    }
    if ( is_null($mappings) ) {
      $mappings = new FieldRoleMappingCollection();
    }
    //Load user to get attached fields.
    $user = user_load($account->uid);
    //Get current domain id.
    $domain_info = domain_get_domain();
    $domain_id = $domain_info['domain_id'];
    //Loop through fields ids in mappings, remember roles.
    $extra_roles = array();
    foreach ( $mappings as $mapping ) {
      $field_name = $mapping->getFieldName();
      //Find that field's data.
      if ( isset($user->$field_name) ) {
        $field_data = $user->$field_name;
        if ( $field_data ) {
          $field_data = $field_data[LANGUAGE_NONE];
          //Is the current domain in the field's data?
          foreach( $field_data as $key => $entry ) {
            if ( $entry['domain_id'] == $domain_id ) {
              //Add the role to the user.
              if ( ! array_key_exists($key, $user->roles ) ) {
                $extra_roles[$mapping->getRoleId()] = $mapping->getRoleName();
              }
            }
          } // end foreach domain in the field.
        }
      }
    } // end foreach mapping
    //Allow other modules to add roles.
    drupal_alter('domrefrole_extra_roles', $extra_roles);
      //Hook must end in _alter.
    //Store in cache
    $extra_roles_cache[$account->uid] = $extra_roles;
    return $extra_roles;
  } catch (Exception $e) {
    domain_reference_roles_log_exception('Domain reference roles', $e);
  }
}

/**
 * Log an exception, return to home page.
 * @param Exception $e Exception
 */
function domain_reference_roles_log_exception(Exception $e) {
  watchdog('Domain reference roles', $e->getFile() . '<br/> ' . $e->getLine() . '<br/> '
      . 'Domain reference roles:' . $e->getMessage()
  );
  drupal_set_message($e->getMessage(), 'error');
  drupal_set_message(t('Error logged.'), 'error');
  //Jump to home page.
  drupal_goto();
}
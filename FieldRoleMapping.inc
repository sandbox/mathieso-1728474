<?php
/**
 * @file
 * Classes encapsulating mappings betwixt domain reference fields and roles.
 */

//module_load_include('inc', 'domain_reference_roles', 'domain_reference_roles.constants');

/**
 * One mapping.
 */
class FieldRoleMapping {
  
  /**
   * Id of a field instance.
   * @var int
   */
  protected $instance_id;
  
  /**
   * Id of a role.
   * @var int
   */
  protected $role_id;
  
  /**
   * Contains metadata about all instances of a field type.
   * @var InstanceMetadataGrabber
   */
  protected $instance_metadata_grabber;
  
  /**
   * Contains data about all user roles, except anon.
   * @var UserRolesGrabber
   */
  protected $user_role_grabber;


  public function __construct( $instance_id, $role_id ) {
    $this->instance_metadata_grabber = 
        InstanceMetadataGrabber::getOneGrabber('domain_reference');
    $this->user_role_grabber = UserRolesGrabber::getOneGrabber();
    $this->setInstanceId($instance_id);
    $this->setRoleId($role_id);
  }
  
  /**
   * Set instance id.
   * @param int $instance_id Field instance id.
   * @throws Exception Bad thing!
   */
  public function setInstanceId($instance_id) {
    if ( !is_int($instance_id) ) {
      throw new Exception(
          'FieldRoleMapping: instance id not an integer: ' . $instance_id .
          ' Check with your dog.'
      );
    }
    if ( ! $this->instance_metadata_grabber->doesInstanceIdExist($instance_id) ) {
      throw new Exception(
          'FieldRoleMapping: instance id does not exist: ' . $instance_id .
          ' Check with your dog.'
      );
    }
    $this->instance_id = $instance_id;
  }
  
  /**
   * Get instance id.
   * @return int Instance id.
   */
  public function getInstanceId() {
    return $this->instance_id;
  }
  
  /**
   * Get the name of the field this instance is an instance of.
   * This is the machine name of the field, not the label.
   * @return string Field name or NULL if not found.
   */
  public function getFieldName() {
    return $this->instance_metadata_grabber->getFieldName($this->instance_id);
  }

  /**
   * Get the name of the instance's entity.
   * @return string Name.
   */
  public function getEntityName() {
    return $this->instance_metadata_grabber->getEntityName($this->instance_id);
  }

  /**
   * Get the name of the instance's bundle.
   * @return string Name.
   */
  public function getBundleName() {
    return $this->instance_metadata_grabber->getBundleName($this->instance_id);
  }

  /**
   * Get the label of the instance's bundle.
   * @return string Label.
   */
  public function getBundleLabel() {
    return $this->instance_metadata_grabber->getBundleLabel($this->instance_id);
  }

  /**
   * Get the label of the instance.
   * @return string Label.
   */
  public function getInstanceLabel() {
    return $this->instance_metadata_grabber->getInstanceLabel($this->instance_id);
  }

  /**
   * Set role id.
   * @param int $role_id Role id.
   * @throws Exception Bad thing!
   */
  public function setRoleId($role_id) {
    if ( ! is_int($role_id) ) {
      throw new Exception(
          'FieldRoleMapping: role id not an integer: ' . $role_id .
          ' Check with your dog.'
      );
    }
    if ( ! $this->user_role_grabber->doesRoleIdExist($role_id) ) {
      throw new Exception(
          'FieldRoleMapping: role id does not exist: ' . $role_id .
          ' Check with your dog.'
      );
    }
    $this->role_id = $role_id;
  }
  
  /**
   * Get a role's name.
   * @return string Role name.
   */
  public function getRoleName() {
    return $this->user_role_grabber->getRoleName($this->role_id);
  }
  
  /**
   * Get role id.
   * @return int role id.
   */
  public function getRoleId() {
    return $this->role_id;
  }
  
}

/**
 * A collection of mappings. Iterable. (Is that really a word?)
 */
class FieldRoleMappingCollection implements Iterator {
  
  const storageVariableName = 'domain_reference_roles_mappings';
  
  //Array of FieldRoleMapping objects.
  protected $mappings = array();
  
  //Object storing metadata about domain_reference field instances.
  protected $instance_metadata_grabber = NULL;
  
  //Object storing data about user roles, except anon.
  protected $role_metadata_grabber;
  
  //Flag set for testing. Loads fake data.
  public static $testing = FALSE;
  
  //Simulates responses to Drupal calls.
  public static $testing_data;
  
  //Simulates persistent storage for testing.
  public static $testing_persistent_storage;
  
  /**
   * Load mapping data from Drupal variable, prepare collection of mapping objects.
   */
  public function __construct() {
    //Get metadata about field instances.
    $this->instance_metadata_grabber = 
        InstanceMetadataGrabber::getOneGrabber('domain_reference');
    //Get existing roles - drop anon.
    $this->role_metadata_grabber = UserRolesGrabber::getOneGrabber();
    $this->loadMappings();
  } // End constructor

  /**
   * Store the mappings in a Drupal variable.
   */
  public function storeMappings() {
    //Build a storage array.
    $storage_array = array();
    $index = 0;
    foreach ( $this->mappings as $mapping ) {
      $storage_array[$index++] = array(
        'instance_id' => $mapping->getInstanceId(),
        'role_id' => $mapping->getRoleId(),
      );
    }
    if ( ! FieldRoleMappingCollection::$testing ) {
      variable_set( FieldRoleMappingCollection::storageVariableName, $storage_array );
    } 
    else {
      FieldRoleMappingCollection::$testing_persistent_storage = $storage_array;
    }
  }
  
  /**
   * Load mappings from storage. 
   */
  protected function loadMappings() {
    $stored_mappings = ( ! FieldRoleMappingCollection::$testing ) 
        ? variable_get( FieldRoleMappingCollection::storageVariableName, array() )
        : FieldRoleMappingCollection::$testing_data['initial_mappings_storage'];
    if ( sizeof($stored_mappings) > 0 ) {
      foreach ( $stored_mappings as $mapping ) { 
        if ( !isset($mapping['instance_id']) || !isset($mapping['role_id']) ) {
          throw new Exception(
              'FieldRoleMappingCollection.loadMappings: ' . t('Bad mapping')
              . ': ' . print_r($mapping, TRUE)
          );
        }
        $instance_id = $mapping['instance_id'];
        $role_id = $mapping['role_id'];
        //Ensure field and role exist. Don't throw exception - role or field
        //  instance might have been deleted.
        $instance_id_exists = 
            $this->instance_metadata_grabber->doesInstanceIdExist($instance_id);
        $role_id_exists = 
            $this->role_metadata_grabber->doesRoleIdExist($role_id);
        if ( $instance_id_exists && $role_id_exists ) {
          //Add new mapping.
          $this->mappings[] = new FieldRoleMapping(
              $instance_id, 
              $role_id
          );
        }
      }//End foreach mapping.
    } //End if there are stored mappings.
  }
  
  /**
   * Count the number of mappings.
   * @return integer Number of mappings.
   */
  public function countMappings() {
    return sizeof($this->mappings);
  }
  
  /**
   * Get a mapping given field instance id, and role id.
   * @param integer $instance_id Mapping's field instance id.
   * @param integer $role_id Mapping's role id.
   * @return Mapping, or null if not found.
   */
  public function getMapping($instance_id, $role_id) {
    foreach ( $this->mappings as $mapping ) {
      if (    $mapping->getInstanceId() == $instance_id 
           && $mapping->getRoleId() == $role_id ) {
        return $mapping;
      }
    }
    return NULL;
  }

  /**
   * Get a mapping given field instance id.
   * @param integer $instance_id Mapping's field instance id.
   * @return Mapping, or null if not found.
   */
  public function getMappingForInstance($instance_id) {
    foreach ( $this->mappings as $mapping ) {
      if ( $mapping->getInstanceId() == $instance_id ) {
        return $mapping;
      }
    }
    return NULL;
  }

  /**
   * Get the roles that are mapped to fields.
   * @return array Roles. Format: role_id => role_name.
   */
  public function getRolesMappedToFields() {
    $roles_mapped_to_fields = array();
    foreach ( $this->mappings as $mapping ) {
      $role_id = $mapping->getRoleId();
      if ( ! array_key_exists( $role_id, $roles_mapped_to_fields ) ) {
        $roles_mapped_to_fields[ $role_id ] = $mapping->getRoleName();
      }
    }
    return $roles_mapped_to_fields;
  }
  
  /**
   * Get the fields that are mapped to roles.
   * @return array Field instances.
   */
  public function getFieldsMappedToRoles() {
    $fields_mapped_to_roles = array();
    foreach ( $this->mappings as $mapping ) {
      $instance_id = $mapping->getInstanceId();
      if ( ! array_key_exists( $instance_id, $fields_mapped_to_roles ) ) {
        $fields_mapped_to_roles[ $instance_id ] = $mapping;
      }
    }
    return $fields_mapped_to_roles;
  }

  /**
   * Check whether a mapping already exists.
   * @param integer $instance_id Field instance id
   * @param integer $role_id Role id
   * @return boolean True if the mapping exists.
   */
  public function mappingExists($instance_id, $role_id) {
    //Find all mappings for the field.
    $field_mappings = array();
    foreach ( $this->mappings as $mapping ) {
      if ( 
             $mapping->getInstanceId() == $instance_id
          && $mapping->getRoleId() == $role_id 
      ) {
        return TRUE;
      }
    } //End foreach.
    return FALSE;
  }
  
  /**
   * Add a new mapping to the collection.
   * @param integer $instance_id Field instance id
   * @param integer $role_id Role id
   * @throws Exception When mapping exists, or field instance id or role id are bad.
   */
  public function addMapping($instance_id, $role_id) {
    if ( ! $this->instance_metadata_grabber->doesInstanceIdExist($instance_id) ) {
      throw new Exception(
          'FieldRoleMappingCollection.addMapping: ' . 
          t('Field instance does not exist. Check with your dog.') . 
          " Instance id: $instance_id  Role id: $role_id"
      );
    }
    if ( ! $this->role_metadata_grabber->doesRoleIdExist($role_id) ) {
      throw new Exception(
          'FieldRoleMappingCollection.addMapping: ' . 
          t('Role does not exist. Check with your dog.') . 
          " Instance id: $instance_id  Role id: $role_id"
      );
    }
    if ( $this->mappingExists($instance_id, $role_id) ) {
      throw new Exception(
          'FieldRoleMappingCollection.addMapping: ' . t('Trying to add a mapping that already exists. Sad dog.') . 
          " Instance id: $instance_id  Role id: $role_id"
      );
    }
    $mapping = new FieldRoleMapping(
        $instance_id,
        $role_id
    );
    $this->mappings[] = $mapping;
  }
  
  /**
   * Remove a mapping from the collection.
   * @param integer $instance_id Field instance id
   * @param integer $role_id Role id
   * @throws Exception When mapping not found.
   */
  public function eraseMapping($instance_id, $role_id) {
    if ( ! $this->mappingExists($instance_id, $role_id) ) {
      throw new Exception(
          'FieldRoleMappingCollection.eraseMapping: ' . t('Trying to erase a nonexistent mapping. Sad dog.') . 
          " Instance id: $instance_id  Role id: $role_id"
      );
    }
    foreach ( $this->mappings as $key => $mapping) {
      if (    $mapping->getInstanceId() == $instance_id 
           && $mapping->getRoleId() == $role_id ) {
        unset($this->mappings[$key]);
        return;
      }
    } //end foreach
    throw new Exception(
        'FieldRoleMappingCollection.eraseMapping: ' . t('Got to end of loop! Get another dog!') . 
        " Instance id: $instance_id  Role id: $role_id"
    );
  }
    

  /**
   * Erase the existing mapppings.
   */
  public function eraseAllMappings() {
    $this->mappings = array();
    $this->storeMappings();
  }

  //Implement methods of iterator interface.
  //See http://php.net/manual/en/language.oop5.iterations.php.
  public function rewind() {
    reset($this->mappings);
  }
  public function current() {
    return current($this->mappings);
  }
  public function key() {
    return key($this->mappings);
  }
  public function next() {
    return next($this->mappings);
  }
  public function valid() {
    $key = key($this->mappings);
    return ($key !== NULL && $key !== FALSE);
  }

}

<?php
/**
 * @files
 * Stores user roles, excluding anon.
 * Caches them for efficiency. See http://drupal.org/node/6463
 */
class UserRolesGrabber {
  /**
   * Singleton pattern. 
   * See http://www.ibm.com/developerworks/library/os-php-designptrns/
   */
  public static $instance = NULL;

  /**
   * Cache for the loaded user role data.
   * @var array
   */
  protected $user_roles = NULL;

  //Flag set for testing. Loads fake data.
  public static $testing = FALSE;
  
  //Simulates responses to Drupal calls.
  public static $testing_data;
  
  /**
   * Make an UserRolesGrabber object.
   * @return UserRolesGrabber object.
   */
  public static function getOneGrabber() {
    if( ! isset(self::$instance) ) {
      self::$instance = new UserRolesGrabber();
    }
    return self::$instance;
  }
  
  /**
   * Constructor not callable outside class.
   */
  protected function __construct() {
    $this->user_roles = ( ! UserRolesGrabber::$testing ) 
        ? user_roles(TRUE)
        : UserRolesGrabber::$testing_data['user_roles'];
  }
  
  /**
   * Return array of roles.
   * @return array Role data.
   */
  public function getAllRoles() {
    return $this->user_roles;
  }

  /**
   * Does a given role id exist?
   * @param int $role_id Role id.
   * @return boolean True if exists, else false.
   */
  public function doesRoleIdExist($role_id) {
    return array_key_exists($role_id, $this->user_roles);
  }
  
  /**
   * Return the name of a role.
   * @param int $role_id Role id.
   * @return string Name.
   */
  public function getRoleName($role_id) {
    return $this->doesRoleIdExist($role_id) 
        ? $this->user_roles[$role_id]
        : NULL;
  }
  
  /**
   * Return the number of roles.
   * @return int Number of roles.
   */
  public function countRoles() {
    return sizeof($this->user_roles);
  }
  
}
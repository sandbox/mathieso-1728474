<?php
/**
 * @file
 * Defines constants for the domain assign roles module.
 */

/**
 * Drupal variable storing the role -> role mappings.
 */
define('DOMAIN_REFERENCE_ROLES_ASSIGN_VAR_NAME', 'domain_reference_roles_assign_mappings');

/**
 * Default path for the user's role management page.
 */
define('domain_reference_roles_ASSIGN_DEFAULT_MANAGEMENT_PATH', 'manage-site/assign-roles');

/**
 * Drupal variable storing the path for the current role management page.
 */
define('domain_reference_roles_ASSIGN_MANAGEMENT_PATH_VAR_NAME', 
    'domain_reference_roles_assign_management_path');

/**
 * Name of the entity storing assigned roles.
 */
//define('domain_reference_roles_ASSIGN_ENTITY_NAME', 'assigned_domain_role');

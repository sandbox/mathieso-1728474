<?php
/**
 * @file
 * Give users an interface to manage assignments of other users to roles on 
 * the current domain.
 */

/**
 * Build form to let users associating users and roles for the current domain.
 */
function _dra_management_form() {
  try {
    $form = array();
    $form['instructions'] = array(
      '#markup' => '<p>' . t('Some users can give roles to other users.') . '</p>',
    );
    //Need to make a table, for the current domain, users and the roles they have, 
    //but only for roles that the current user is able to assign.
    //How?
    //1. Get the roles the current user can assign.
    //2. Get the fields that reference those roles.
    //3. Get the users who have the current domain id in one of those fields.
    //For each user:
    //  A: Get the extra roles the user has.
    //  B: Find which of those roles the current user can grant.
    //  C: For each role, show a user/role combination.
    
    //Get current domain.
    $domain_info = domain_get_domain();
    $domain_id = $domain_info['domain_id'];
    //Get the assigner role -> assignee role mappings.
    //Load the current assigner role -> assignee role mappings
    $assigner_assignee_role_mappings = new AssignerAssigneeRoleMappingCollection();
    if ( $assigner_assignee_role_mappings->countMappings() == 0 ) {
      $form['error'] = array(
        '#markup' => 
            '<p>' . t('There are no roles available for this purpose.') . '</p>
             <p>' . t('Check with your site administrator for details.') . '</p>',
      );
      return $form;
    }
    $field_role_mappings = new FieldRoleMappingCollection();
    //1. Get the roles the user can assign.
    global $user;
    $roles_can_assign = 
      _dra_get_roles_user_can_assign(
          $user, 
          $assigner_assignee_role_mappings,
          $field_role_mappings
      );
    if ( sizeof($roles_can_assign) == 0 ) {
      $form['error'] = array(
        '#markup' => 
            '<p>' . t('Sorry, your are not able to assign roles.') . '</p>
             <p>' . t('Check with your site administrator for details.') . '</p>',
      );
      return $form;
    }
    //Add the roles to the form to save time in validation.
    $form['roles_user_can_assign'] = array(
      '#type' => 'value',
      '#value' => $roles_can_assign,
    );
    //2. Find the fields that assign those roles.
    $fields_assigning_roles = 
        _dra_find_fields_assigning_roles(
            $roles_can_assign,
            $field_role_mappings
        );
    if ( sizeof($fields_assigning_roles) == 0 ) {
      $form['error'] = array(
        '#markup' => 
            '<p>' . t('Sorry, there are no domain reference fields controlling roles ' .
                      'you are able to assign.') . '</p>
             <p>' . t('Check with your site administrator for details.') . '</p>',
      );
      return $form;
    }
    //3. Get users who have the current domain id in one of those fields. The 
    //current user is able to delete at least one of those assignments.
    $instance_metadata_grabber = InstanceMetadataGrabber::getOneGrabber('domain_reference');
    $user_ids = array();
    foreach ( $fields_assigning_roles as $instance_id => $role_id ) {
      $field_name = $instance_metadata_grabber->getFieldName($instance_id);
      $query = new EntityFieldQuery();
      $query
          ->entityCondition('entity_type', 'user')
          ->entityCondition('bundle', 'user')
          ->fieldCondition($field_name, 'domain_id', $domain_id, '=')
          ->addMetaData('account', user_load(1)); // Run the query as user 1.
          ;
      $result = $query->execute();
      //Anything returned?
      if ( isset( $result['user'] ) ) {
        foreach ( $result['user'] as $result_record ) {
          $uid = $result_record->uid;
          if ( !in_array($uid, $user_ids)  ) {
            $user_ids[] = $uid;
          }
        }
      } //End there are results.
    }
    if ( sizeof( $user_ids ) == 0 ) {
      $form['no_roles'] = array(
        '#markup' => 
            '<p>' . t('(No roles have been assigned yet.)') . '</p>'
      );
    }
    else {
      $form['table_intro'] = array(
        '#markup' => 
            '<p>' . t('Here are role assignments you can change.') . '</p>'
      );
      $user_role_grabber = UserRolesGrabber::getOneGrabber();
      //User/role combinations the logged in user can delete. There will be
      //  a table row for each one on the form.
      $user_role_records = array();
      //For each user...
      foreach ( $user_ids as $uid ) {
        //Get the extra roles the user has. 
        $temp_user = user_load($uid);
        $extra_roles = domain_reference_roles_get_extra_roles($temp_user, $field_role_mappings);
        //Which of those roles can the current user assign?
        foreach ( $extra_roles as $extra_role_id => $extra_role_name ) {
          if ( isset( $roles_can_assign[$extra_role_id] ) ) {
            //User can assign this role. 
            //Make sure the role exists.
            if ( $user_role_grabber->doesRoleIdExist($extra_role_id) ) {
              //Remember the user/role combination.
              $user_role_records[] = 
                  array (
                    'uid' => $uid,
                    'un' => $temp_user->name,
                    'rid' => $extra_role_id,
                    'rn' => $user_role_grabber->getRoleName($extra_role_id),
                  );
            } //End role exists.
          } //End user can assign role.
        } //End for each extra role a user has.
      } //End for each user.
      //Sort the user/role list, by role name within user name.
      usort($user_role_records, 'compare_user_role_records');
      //Time to make the table.
      $headers = array('User', 'Role', 'Operations');
      $rows = array();
      $delete_path = _dra_get_current_manage_path() . '/delete/';
      foreach ( $user_role_records as $user_role_record ) {
        $row = array();
        $row[] = $user_role_record['un']; //User name.
        $row[] = $user_role_record['rn']; //Role name.
        $row[] = l(
            'Delete', 
            $delete_path . $user_role_record['uid'] . '/' . $user_role_record['rid']
        );
        $rows[] = $row;
      } //End for each user/role record.
      $form['users_roles'] = array(
        '#theme' => 'table',
        '#rows' => $rows,
        '#header' => $headers,
      );        
    } //End number of users not zero.
    //Output controls for adding a new record.
    $form['assign_new_intro'] = array(
      '#markup' => 
          '<p>' . t('You can assign a new role to a user.') . '</p>'
    );
    //User
    $form['name_user_to_get_role'] = array(
      '#type' => 'textfield',
      '#required' => TRUE,
      '#title' => t('User'),
      '#description' => 
          t('User who will get a new role. Only one user name at a time. ' .
            'Hint: start typing a user name.'),
      // The autocomplete path is provided in hook_menu in ajax_example.module.
      '#autocomplete_path' => 'user/autocomplete',
    );  
    //Role
//    $role_options = $user_role_grabber->getAllRoles();
    $form['new_role_id'] = array(
      '#type' => 'select',
      '#required' => TRUE,
      '#title' => t('Assignee role'),
      '#description' => t('Role assigned to users'),
      '#options' => $roles_can_assign,
    );
    $form['submit'] = array(
      '#type' => 'submit',
      '#value' => 'Save',
    );
    return $form;
  } catch (Exception $e) {
    domain_reference_roles_assign_log_exception( $e );
  }
}

/**
 * Find the roles a user can assign on the current domain.
 * @param stdClass $user User to check.
 * @param AssignerAssigneeRoleMappingCollection $assigner_assignee_role_mappings
 *    Current mappings. Passed for performance. 
 * @param FieldRoleMappingCollection $field_role_mappings Current collection.
 *    Passed for performance, to avoid recomputing it.
 * @return array Roles user can assign, could be MT. Format: role_id => role_name
 */
function _dra_get_roles_user_can_assign(
    $user, 
    $assigner_assignee_role_mappings,
    $field_role_mappings
  ) {
  //Get the users' extra roles assigned by domain role access.
  //  Roles given by other means are not included here.
  $extra_roles = domain_reference_roles_get_extra_roles($user, $field_role_mappings);
  //Find which of those roles are assigner roles.
  $current_user_assigner_mappings = array();
  foreach ( $assigner_assignee_role_mappings as $role_mapping ) {
    $assigner_role_id = $role_mapping->getAssignerRoleId();
    if ( array_key_exists($assigner_role_id, $extra_roles ) ) {
      //Current user has an assigner role.
      $current_user_assigner_mappings[] = $role_mapping;
    }
  }
  //Now we have a set of assigner/assignee mappings.
  //Extract roles the current user can assign.
  $roles_can_assign = array();
  foreach ( $current_user_assigner_mappings as $mapping ) {
    $assignee_role_id = $mapping->getAssigneeRoleId();
    $assignee_role_name = $mapping->getAssigneeRoleName();
    if ( !array_key_exists($assignee_role_id, $roles_can_assign) ) {
      $roles_can_assign[$assignee_role_id] = $assignee_role_name;
    }
  }
  return $roles_can_assign;
}

/**
 * Find fields that can assign certain roles.
 * @param array $roles_can_assign Roles to check. role_id => role_name.
 * @param FieldRoleMappingCollection $field_role_mappings Current collection.
 *    Passed for performance, to avoid recomputing it.
 * @return array Fields that grant those roles. field_instance_id => role_id.
 */
function _dra_find_fields_assigning_roles ( $roles_can_assign, $field_role_mappings ) {
  $fields_granting_roles = array();
  foreach ( $field_role_mappings as $field_role_mapping ) {
    $role_id = $field_role_mapping->getRoleId();
    $field_id = $field_role_mapping->getInstanceId();
    if (array_key_exists( $role_id, $roles_can_assign ) ) {
      //The role in the field/role mapping is one the current user can assign.
      //Remember the field.
      if ( ! array_key_exists($field_id, $fields_granting_roles) ) {
        $fields_granting_roles[ $field_id ] = $role_id;
      }
    }
  }
  return $fields_granting_roles;
}

/**
 * Callback for sorting user/role records.
 * @param array $a A user/role record.
 * @param type $b Another user/role record.
 * @return int -1, 0, 1
 */
function compare_user_role_records( $a, $b ) {
  $a_un = strtolower($a['un']);
  $b_un = strtolower($b['un']);
  if ( $a_un > $b_un ) {
    return 1;
  }
  if ( $a_un < $b_un ) {
    return -1;
  }
  $a_rn = strtolower($a['rn']);
  $b_rn = strtolower($b['rn']);
  if ( $a_rn > $b_rn ) {
    return 1;
  }
  if ( $a_rn < $b_rn ) {
    return -1;
  }
  return 0;
}

/**
 * Validate form submission.
 */
function _dra_management_form_validate($form, &$form_state) {
  try {
    //Get the logged in user.
    global $user;
    //Check the user selected.
    $name_user_to_get_role = trim($form_state['values']['name_user_to_get_role']);
    if ( $name_user_to_get_role == '') {
      //User left the field blank.
      form_set_error('name_user_to_get_role', t('Sorry, you must enter a user name.') );
      return;
    }
    //Tried to enter multiple users?
    if (    strstr($name_user_to_get_role, ',') !== FALSE 
         || strstr($name_user_to_get_role, ' ') !== FALSE  ) {
      form_set_error('name_user_to_get_role', 
          t('Sorry, you can only enter one user name at a time.') );
      return;
    }
    //Get the user's id.
    $user_to_get_role = user_load_by_name($name_user_to_get_role);
    //Make sure user exists.
    if ( $user_to_get_role === FALSE ) {
      form_set_error('name_user_to_get_role', 
          t("Sorry, the user \"$name_user_to_get_role\" was not found.") );
      return;
    }
    $id_user_to_get_role = $user_to_get_role->uid;
//    if ( $id_user_to_get_role == $user->uid ) {
//      form_set_error(
//          'name_user_to_get_role', 
//          t('Sorry, you can\'t give yourself a new role.') 
//      );
//      return;
//    }
    //Keep user data for the form submit step.
    $form_state['values']['user_id'] = $id_user_to_get_role;
    $form_state['values']['user_name'] = $name_user_to_get_role;
    //Check the role selected.
    $new_role_id = (int) $form_state['values']['new_role_id'];
    if ( ! $new_role_id ) {
      return;
    }
    //Get the roles the current user can assign.
    $roles_can_assign = $form_state['values']['roles_user_can_assign'];
    //Check that the user can assign the role.
    if ( ! array_key_exists($new_role_id, $roles_can_assign) ) {
      throw new Exception(
          '_dra_management_form_validate: User cannot assign role: ' . $new_role_id
      );
    }
    //Check whether this role/role combination is already in the list for 
    //this domain.
    $field_role_mappings = new FieldRoleMappingCollection();
    $extra_roles = domain_reference_roles_get_extra_roles($user_to_get_role, $field_role_mappings);
    if ( array_key_exists($new_role_id, $extra_roles) ) {
      form_set_error('name_user_to_get_role', 'Sorry, the user ' . 
          $name_user_to_get_role . ' already has the role ' . 
          $extra_roles[$new_role_id] .'.'
      );
    }
    //Check that there is a field on the user object that will assign the role.
    //There should be, given the process run so far. Can you say, "Paranoia?"
    $found_field = FALSE;
    foreach ( $field_role_mappings as $field_role_mapping ) {
      if ( 
               $field_role_mapping->getEntityName() == 'user'
            && $field_role_mapping->getBundleName() == 'user'
            && $field_role_mapping->getRoleId() == $new_role_id
         ) {
        //Store the field and role name for the submit step.
        $form_state['values']['field_name'] = $field_role_mapping->getFieldName();
        $form_state['values']['role_name'] = $field_role_mapping->getRoleName();
        $found_field = TRUE;
        break;
      }
    }
    if ( ! $found_field ) {
      throw new Exception(
          '_dra_management_form_validate: Field cannot assign role: ' . $new_role_id
      );
    }
    //Warn if role is administrator.
    $admin_roles = user_roles(TRUE, 'administer modules');
    if ( array_key_exists($new_role_id, $admin_roles) ) {
      drupal_set_message( 
          t('Warning: You have assigned access to an administrator role.'), 
          'warning'
      );
    }
  } catch (Exception $e) {
    domain_reference_roles_assign_log_exception( $e );
  }
}


/**
 * Handle field/role mapping form submission.
 */
function _dra_management_form_submit($form, &$form_state) {
  try {
    $user_id = $form_state['values']['user_id'];
    $field_name = $form_state['values']['field_name'];
    //Get current domain.
    $domain_info = domain_get_domain();
    $domain_id = $domain_info['domain_id'];
    //Save.
    $target_user = user_load($user_id);
    //Values array - need it because PHP deferences 
    //  $target_user->$field_name[LANGUAGE_NONE] incorrectly.
    $values = $target_user->$field_name;
    if ( sizeof( $target_user->$field_name ) == 0 ) {
      //This is the only element.
      $values[LANGUAGE_NONE][0]['domain_id'] = $domain_id;
//      $user->$field_name[LANGUAGE_NONE][0]['domain_id'] = $domain_id;
    }
    else {
      //Add to existing entries.
      $num_entries = sizeof( $values[LANGUAGE_NONE] );
//      $num_entries = sizeof( $user->$field_name[LANGUAGE_NONE] );
      $values[LANGUAGE_NONE][$num_entries]['domain_id'] = $domain_id;
//      $user->$field_name[LANGUAGE_NONE][$num_entries]['domain_id'] = $domain_id;
    }
    $target_user->$field_name = $values;
    user_save($target_user);
    drupal_set_message(
        'Added role ' . $form_state['values']['role_name'] . 
        ' for the user ' . $form_state['values']['user_name'] . '.'
    );
  } catch (Exception $e) {
    domain_reference_roles_assign_log_exception( $e );
  }
}

/**
 * Returns HTML for the domain reference roles admin form.
 *
 * @param $variables
 *   An associative array containing:
 *   - form: A render element representing the form.
 *
 * @ingroup themeable
 */
//function theme__dr_field_role_form($variables) {
//  try {
//    $form = $variables['form'];
//    if (!empty($form['error'])) {
//      $output = drupal_render($form['error']);
//      return $output;
//    }
//    $output = '';
//    $output .= drupal_render($form['instructions']);
//    if ( isset($form['no_mappings']) ) {
//      $output .= drupal_render($form['no_mappings']);
//    }
//    if ( isset($form['mappings']) ) {
//      $header = array(
//        t('Domain reference field'), 
//        t('Role'),
//        t('Operation'),
//      );
//      $rows = array();
//      foreach ( element_children($form['mappings']) as $key ) {
//        $row_output = array();
//        $row_elements = element_children($form['mappings'][$key]);
//        foreach ( $row_elements as $item ) {
//          $rendered = drupal_render($form['mappings'][$key][$item]);
//          $row_output[] = array(
//            'data' => $rendered,
//            'class' => array($item),
//          );
//        }
//        $rows[] = array(
//          'data' => $row_output,
//          'class' => array('domain_reference_roles_field_role_mapping'),
//        );
//      }
//      $attributes = array(
//        'id' => 'domain_reference_roles_field_role_mappings',
//      );
//      $output .= theme('table', array(
//          'header' => $header, 
//          'rows' => $rows, 
//          'attributes' => $attributes,
//      ));
//    }
//    $output .= drupal_render_children($form);
//    return $output;
//  } catch (Exception $e) {
//    domain_reference_roles_assign_log_exception( $e );
//  }
//}

/**
 * Form callback for delete confirmation form.
 * @param array $form Form definition.
 * @return array Form renderable array.
 * @throws Exception If something strange and bad happens. Get another dog.
 */
function _dra_confirm_role_assign_delete($form, &$form_state) {
  try {
    //Get the user/role combination to remove.
    $path_pieces = explode( '/', current_path() );
    $target_user_id = $path_pieces[ sizeof($path_pieces) - 2 ];
    $target_role_id = $path_pieces[ sizeof($path_pieces) - 1 ];
    //Check user and role - exception thrown if Evil detected.
    _dra_check_delete_request($target_user_id, $target_role_id);
    $target_user = user_load($target_user_id);
    $user_roles_grabber = UserRolesGrabber::getOneGrabber();
    //Return confirmation form.
    $question = 'Confirm deletion';
    $message = 
      '<p><strong>' . t('Are you sure you want to delete this assignment?') . '</strong></p>
       <ul>
         <li>' . t('User:') . ' ' . $target_user->name . '</li>
         <li>' . t('Role:') . ' ' . $user_roles_grabber->getRoleName($target_role_id) . '</li>
       </ul>
       <p>' . t('This action cannot be undone.') . '</p>';
    return confirm_form($form, $question, _dra_get_current_manage_path(), 
        $message, 'Delete', 'Cancel', 'role_assign_delete_confirmation_item');
  } catch (Exception $e) {
    domain_reference_roles_assign_log_exception( $e );
  }
}

/**
 * Check the user and role ids passed to a delete page.
 * @global stdClass $user Logged in user.
 * @param int $target_user_id User id.
 * @param int $target_role_id Role id.
 * @throws Exception Something bad happened.
 */
function _dra_check_delete_request($target_user_id, $target_role_id) {
  global $user;
  //Check the user id.
  if ( $target_user_id == 1 ) {
    throw new Exception('_dra_check_delete_request: Attempt to mess with ' . 
        'user 1. Logged in user: ' . $user->uid);
  }
  $target_user = user_load($target_user_id);
  if ( $target_user === FALSE ) {
    throw new Exception('_dra_check_delete_request: User id not found. ' .
        "User id: $target_user_id  Logged in user: " . $user->uid);
  }
  //Check the role id.
  $user_roles_grabber = UserRolesGrabber::getOneGrabber();
  if ( ! $user_roles_grabber->doesRoleIdExist($target_role_id) ) {
    throw new Exception('_dra_check_delete_request: Attempt to delete ' .
        'assignment for a role that does not exist. ' .
        "User id: $target_user_id  Role id: $target_role_id Logged in user: " . 
        $user->uid);
  }
  //Check that the current user can delete that role.
  $assigner_assignee_role_mappings = new AssignerAssigneeRoleMappingCollection();
  $field_role_mappings = new FieldRoleMappingCollection();
  $roles_can_assign = 
    _dra_get_roles_user_can_assign(
        $user, 
        $assigner_assignee_role_mappings,
        $field_role_mappings
    );
  if ( ! array_key_exists($target_role_id, $roles_can_assign) ) {
    throw new Exception('_dra_check_delete_request: Attempt to delete ' .
        'assignment for a role the user does not have access to. ' .
        "User id: $target_user_id  Role id: $target_role_id   Logged in user: " . 
        $user->uid);
  }
}

/**
 * User/role deletion confirmed delete.
 * @param array $form Form definition.
 * @param array $form_state Form state.
 */
function _dra_confirm_role_assign_delete_submit($form, &$form_state) {
  try {
    //Get the user/role combination to remove.
    $path_pieces = explode( '/', current_path() );
    $target_user_id = $path_pieces[ sizeof($path_pieces) - 2 ];
    $target_role_id = $path_pieces[ sizeof($path_pieces) - 1 ];
    //Check user and role - exception thrown if Evil detected.
    _dra_check_delete_request($target_user_id, $target_role_id);
    //Delete the domain id from domain reference fields granting the target role.
    //Get current domain.
    $domain_info = domain_get_domain();
    $domain_id = $domain_info['domain_id'];
    //Get the fields that are mapped.
    $field_role_mappings = new FieldRoleMappingCollection();
    //Find the fields that can map the role to be removed.
    $field_names = array();
    foreach ( $field_role_mappings as $field_role_mapping ) {
      if ( 
             $field_role_mapping->getEntityName() == 'user'
          && $field_role_mapping->getBundleName() == 'user' 
          && $field_role_mapping->getRoleId() == $target_role_id 
         ) {
           $field_names[] = $field_role_mapping->getFieldName();
      }
    }
    $target_user = user_load($target_user_id);
    //Go through the fields, and remove references to the domain.
    foreach ( $field_names as $field_name ) {
      if ( sizeof( $target_user->$field_name ) == 0 ) {
        //No values - move on.
        continue;
      }
      //Values array - need it because PHP deferences 
      //  $target_user->$field_name[LANGUAGE_NONE] incorrectly.
      $values = $target_user->$field_name;
//      kpr('aa ' . sizeof( $aa ) );
//      kpr('aa1 ' . sizeof( $aa[LANGUAGE_NONE] ) );
//      kpr('rr ' . sizeof($target_user->$field_name[LANGUAGE_NONE]) );
//      kpr('ss ' . sizeof($target_user->$field_name[LANGUAGE_NONE]) );
//      kpr('tt ' . sizeof($target_user->$field_name['und']) );
//      $x = $target_user->$field_name;
//      $y = $x[LANGUAGE_NONE];
//      dpr('vv '. sizeof($y));
//      $target_field_name = $target_user->$field_name;
//      $target_field_name_language = $target_field_name[LANGUAGE_NONE];
//      if ( sizeof( $target_field_name_language ) == 1 ) {
      if ( sizeof( $values[LANGUAGE_NONE] ) == 1 ) {
//      if ( sizeof( $target_user->$field_name[LANGUAGE_NONE] ) == 1 ) {
        //Just one value. Is it the one to remove?
//        if ( $target_user->$field_name[LANGUAGE_NONE][0]['domain_id'] 
        if ( $values[LANGUAGE_NONE][0]['domain_id'] == $domain_id ) {
          $target_user->$field_name = array();
          continue;
        }
      } //End one value.
      else {
        //There's more than one value. Loop through them.
        $num_elements = sizeof( $values[LANGUAGE_NONE] );
  //      $num_elements = sizeof( $target_user->$field_name[LANGUAGE_NONE] );
        for ( $index = 0; $index < $num_elements; $index++ ) {
          if ( $values[LANGUAGE_NONE][$index]['domain_id'] == $domain_id ) {
  //        if ( $target_user->$field_name[LANGUAGE_NONE][$index]['domain_id'] == $domain_id ) {
            unset( $values[LANGUAGE_NONE][$index] );
            break;
          }
        }
        //Reindex the array and put it back.
        $target_user->$field_name = array_values($values);
      }
    } //End for each field.
//    if ( ! isset( $edit[$field_name][LANGUAGE_NONE][0]['domain_id'] ) ) {
//      $edit[$field_name][LANGUAGE_NONE][0]['domain_id'] = $domain_id;
//    }
//    else {
//      $num_entries = sizeof( $edit[$field_name][LANGUAGE_NONE] );
//      $edit[$field_name][LANGUAGE_NONE][$num_entries]['domain_id'] = $domain_id;
//    }
    user_save($target_user);
    drupal_goto( _dra_get_current_manage_path() );
  } catch (Exception $e) {
    domain_reference_roles_assign_log_exception( $e );
  }
}

<?php
/**
 * @file
 * Defines an admin form for the domain assign roles module.
 */

module_load_include('inc', 'domain_reference_roles', 'domain_reference_roles.constants');
module_load_include('inc', 'domain_reference_roles_assign', 'domain_reference_roles_assign.constants');
module_load_include('inc', 'domain_reference_roles_assign', 'AssignerAssigneeRoleMapping');

/**
 * Build form for associating roles (assigner) and roles (assignee).
 */
function _dra_assigner_assignee_role_form() {
  try {
    $current_path = _dra_get_current_manage_path();
    $form = array();
    $form['#validate'] = array('_dra_assigner_assignee_role_form_validate');
    $form['#submit'] = array('_dra_assigner_assignee_role_form_submit');
    
    $form['instructions'] = array(
      '#markup' => '<p>' . t('Configure your site so that users (e.g., site ' . 
        'managers) can assign roles to other users (e.g., site maintainers) ' .
        'for referenced domains.' ) . '</p>' .
        '<p>' . t('There are two parts to this:') . '</p>' .
        theme('item_list', array(
          'type' => 'ul',
          'items' => array(
            t('Enter the path to the page that privileged users will use to assign roles to ' . 
              'other users. This is called the "management page."'),
            t('Select pairs of assigner and assignee roles. Users with the assigner role ' .
              'can assign the assignee role to other users.' ),
          ),
        ))
    );
    
    $form['management_path'] = array(
      '#type' => 'fieldset',
      '#title' => t('Path to management page'),
    );    
    $form['management_path']['instructions'] = array(
      '#markup' => '<p>' . t('Enter the path to the management page. If you ' .
          'are unsure, use the example below.') .
        '<p>' . t('Example: ') . domain_reference_roles_ASSIGN_DEFAULT_MANAGEMENT_PATH . '</p>
         <p>' . t('Current value: ') . l($current_path, $current_path) . '</p>'
    );
    $form['management_path']['path'] = array(
      '#title' => 'Path',
      '#type' => 'textfield',
      '#description' => 'Enter the path.',
      '#default_value' => $current_path,
      '#required' => TRUE,
    );

    $form['roles_assigning_roles'] = array(
      '#type' => 'fieldset',
      '#title' => t('Roles assigning roles'),
    );
    $form['roles_assigning_roles']['instructions'] = array(
      '#markup' => '<p>' . t('Choose roles that will allow users to assign roles ' .
         'on domains.') . '</p>',
    );
    //Get roles mapped to domain reference fields. They can be used in the 
    //assigner/assignee role mappings.
    $field_role_mappings = new FieldRoleMappingCollection();
    if ( $field_role_mappings->countMappings() == 0 ) {
      $form['roles_assigning_roles']['error'] = array(
        '#markup' => 
            '<p>' . t('There are no roles available for this purpose.') . '</p>
             <p>See ' . l( t('help'), 'admin/help#domain_reference_roles_assign') . 
             t(' for information.') . '</p>',
      );
    }
    else {
      //Load the current assigner role -> assignee role mappings
      $mappings = new AssignerAssigneeRoleMappingCollection();
      //Make mappings ready to show.
      if ( $mappings->countMappings() == 0 ) {
        $form['roles_assigning_roles']['no_mappings'] = array(
          '#markup' => '<p>' . t('(Currently, there are no associations ' . 
              'between assigner and assignee roles. You can add some.)') . '</p>'
        );
      }
      else {
        //Create table of current role -> role mappings.
        $header = array(
          t('Assigner role'), 
          t('Assignee role'),
          t('Operations'),
        );
        $rows = array();
        foreach ( $mappings as $mapping ) {
          $row = array();
          $row['assigner_role_name'] = check_plain( $mapping->getAssignerRoleName() );    
          $row['assignee_role_name'] = check_plain( $mapping->getAssigneeRoleName() );
          $row['delete'] = 
              l( t('Delete'), 
                 'admin/structure/domain/domain-roles/assign-roles/delete/' . 
                  $mapping->getAssignerRoleId() . '/' . 
                  $mapping->getAssigneeRoleId()
          );
          $rows[] = $row;
        }// end foreach mapping.
        $form['roles_assigning_roles']['mappings'] = array(
          '#theme' => 'table',
          '#rows' => $rows,
          '#header' => $header,
        );        
      }
      //Controls for list of roles, for adding a new mapping.
      $roles_mapped_to_fields = $field_role_mappings->getRolesMappedToFields();
      //Add an MT element at the top, for when the user doesn't want to make a
      //selection.
      $roles_mapped_to_fields = array('0' => '') + $roles_mapped_to_fields;
      //Assigner role
      $form['roles_assigning_roles']['new_assigner_role_id'] = array(
        '#type' => 'select',
        '#title' => t('Assigner role'),
        '#description' => t('Users with this role can assign the assignee role.'),
        '#options' => $roles_mapped_to_fields,
      );
      //Assignee role
      $form['roles_assigning_roles']['new_assignee_role_id'] = array(
        '#type' => 'select',
        '#title' => t('Assignee role'),
        '#description' => t('Role assigned to users'),
        '#options' => $roles_mapped_to_fields,
      );
    }
    //Button to process all.
    $form['save'] = array(
      '#type' => 'submit',
      '#value' => t('Save'),
    );
    return $form;
  } catch (Exception $e) {
    domain_reference_roles_assign_log_exception( $e );
  }
}

/**
 * Validate form submission.
 */
function _dra_assigner_assignee_role_form_validate($form, &$form_state) {
  //Only check on path is that it is a required form field.
  try {
    //Check the roles.
    $new_assigner_role_id = (int) $form_state['values']['new_assigner_role_id'];
    $new_assignee_role_id = (int) $form_state['values']['new_assignee_role_id'];
    //If both are MT, then leaave - nothing to check.
    if ( $new_assigner_role_id == 0 && $new_assignee_role_id == 0 ) {
      return;
    }
    //Is one of them MT?
    if ( $new_assigner_role_id == 0 || $new_assignee_role_id == 0 ) {
      form_set_error('new_assigner_role_id', 
          t('Sorry, you must select both assigner and assignee roles.') 
      );
      return;
    }
    //Make sure roles exist and can be used. If not, Big Trouble in Domain assign roles.
    $field_role_mappings = new FieldRoleMappingCollection();
    $available_roles = $field_role_mappings->getRolesMappedToFields();
    if ( ! array_key_exists($new_assigner_role_id, $available_roles) ) {
      throw new Exception('_dra_assigner_assignee_role_form_validate: unexpected assigner role id: ' 
          . $new_assigner_role_id);
    }
    if ( ! array_key_exists($new_assignee_role_id, $available_roles) ) {
      throw new Exception('_dra_assigner_assignee_role_form_validate: unexpected assignee role id: ' 
          . $new_assignee_role_id);
    }
    //Get mapping data.
    $mappings = new AssignerAssigneeRoleMappingCollection();
    //Check whether this field/role combination is already in the list.
    if ( $mappings->mappingExists($new_assigner_role_id, $new_assignee_role_id) ) {
      form_set_error('new_assigner_role_id', 
          t('Sorry, there is already an association between those roles.') 
      );
    }
    //Warn if role is administrator.
    $admin_roles = user_roles(TRUE, 'administer modules');
    if ( array_key_exists($new_assignee_role_id, $admin_roles) ) {
      drupal_set_message( 
          t('Warning: You have assigned access to an administrator role.'), 
          'warning'
      );
    }
  } catch (Exception $e) {
    domain_reference_roles_assign_log_exception( $e );
  }
}

/**
 * Handle field/role mapping form submission.
 */
function _dra_assigner_assignee_role_form_submit($form, &$form_state) {
  try {
    //First save the path.
    $current = _dra_get_current_manage_path();
    $new = $form_state['values']['path'];
    if ( $current != $new ) {
      if ( drupal_valid_path($new) ) {
        drupal_set_message( 
            t('Warning: Drupal was already using the path ' .
              'in the current domain, though maybe not in subdomains. ' . 
              'Consider changing the path. The value given in the example ' .
              'will almost always work.'), 
            'warning'
        );
      }
      variable_set(domain_reference_roles_ASSIGN_MANAGEMENT_PATH_VAR_NAME, $new);
      drupal_set_message( t('New path saved') );
      menu_rebuild();
    }
    //Now save the field mappings.
    $new_assigner_role_id = (int) $form_state['values']['new_assigner_role_id'];
    $new_assignee_role_id = (int) $form_state['values']['new_assignee_role_id'];
    if ( $new_assigner_role_id != 0 && $new_assignee_role_id != 0 ) {
      //Get mapping data.
      $mappings = new AssignerAssigneeRoleMappingCollection();
      //Store the new mapping.
      $mappings->addMapping($new_assigner_role_id, $new_assignee_role_id);
      $mappings->storeMappings();
      drupal_set_message( t('New role assocation added.') );
    }
  } catch (Exception $e) {
    domain_reference_roles_assign_log_exception( $e );
  }
}

/**
 * Form callback for delete confirmation form.
 * @param array $form Form definition.
 * @param array $form_state Form state, e.g., submission values.
 * @param integer $assigner_role_id Id of the assigner role.
 * @param integer $assignee_role_id Id of the assignee role.
 * @return array Form renderable array.
 * @throws Exception If something strange and bad happens. Get another dog.
 */
function _dra_confirm_assigner_assignee_link_delete($form, &$form_state, 
    $assigner_role_id, $assignee_role_id) {
  try {
    //Load mapping data.
    $mappings = new AssignerAssigneeRoleMappingCollection();
    //Check that the passed roles exists.
    if ( ! $mappings->mappingExists($assigner_role_id, $assignee_role_id) ) {
      throw new Exception('domain_reference_roles_assign_confirm_delete_form: Attempt to delete ' .
          '  a mapping that does not exist. Check with your dog. ' .
          " Assigner id: $assigner_role_id  Assignee id: $assignee_role_id"
      );
    } //End check passed mapping exists.
    $mapping = $mappings->getMapping($assigner_role_id, $assignee_role_id);
    //The role ids have already been checked. If they can't be found, 
    //something serious is wrong. Time for a dog alert.
    if ( is_null($mapping) ) {
      throw new Exception('domain_reference_roles_assign_confirm_delete_form: something ' .
        'impossible happened. Keys just checked are bad. Get another dog. ' .
        " Assigner id: $assigner_role_id  Assignee id: $assignee_role_id"
      );
    }
    //Return confirmation form.
    $question = 'Confirm domain role mapping deletion';
    $message = 
      '<p>' . t('Are you sure you want to delete this role assign?') . '</p>' .
      theme('item_list', array(
        'type' => 'ul',
        'items' => array(
          t('Assigner role: ') . $mapping->getAssignerRoleName(),
          t('Assignee role:') . ' ' . $mapping->getAssigneeRoleName(),
        ),
      )) . 
      '<p>' . t('This action cannot be undone.') . '</p>';
    return confirm_form($form, $question, 
        'admin/structure/domain/domain-roles/assign-roles', 
        $message, 'Delete', 'Cancel', 'delete_confirmation_item');
  } catch (Exception $e) {
    domain_reference_roles_assign_log_exception( $e );
  }
}

/**
 * User confirmed delete.
 * This function checks for cases that should be impossible,
 * given all the validation that has occurred already. 
 * The security implications of this module make such paranoia appropriate.
 * @param array $form Form definition.
 * @param array $form_state Form state.
 */
function domain_reference_roles_assign_confirm_delete_form_submit($form, &$form_state) {
  try {
    $confirm = $form_state['values']['delete_confirmation_item'];
    $assigner_role_id = $form_state['build_info']['args'][0];
    $assignee_role_id = $form_state['build_info']['args'][1];
    //Load the mappings.
    $mappings = new AssignerAssigneeRoleMappingCollection();
    //$confirm should be 1. If not, something bad is happening.
    if ( $confirm != 1 ) {
      throw new Exception("Unexpected BAD THING. domain_reference_roles_assign_confirm_delete_form_submit: 
          confirm is not 1. VERY STRANGE!
          Assigner id: $assigner_role_id  Assignee id: $assignee_role_id"
      );
    }
    //Check that the mapping exists.
    if ( ! $mappings->mappingExists($assigner_role_id, $assignee_role_id) ) {
      throw new Exception("domain_reference_roles_assign_confirm_delete_form_submit: 
          mapping does not exist. Was it just deleted?
          Assigner id: $assigner_role_id  Assignee id: $assignee_role_id"
      );
    }
    $mappings->eraseMapping($assigner_role_id, $assignee_role_id);
    $mappings->storeMappings();
    drupal_set_message('Role association deleted.');
    drupal_goto('admin/structure/domain/domain-roles/assign-roles');
  } catch (Exception $e) {
    domain_reference_roles_assign_log_exception( $e );
  }
}

<?php
/**
 * @file
 * Classes encapsulating mappings betwixt roles and roles.
 * Only roles involved in a domain reference field/role mapping are used.
 */

module_load_include('inc', 'domain_reference_roles_assign', 'domain_reference_roles_assign.constants');
module_load_include('inc', 'domain_reference_roles', 'UserRolesGrabber');
module_load_include('inc', 'domain_reference_roles', 'FieldRoleMapping');

/**
 * One mapping.
 */
class AssignerAssigneeRoleMapping {
  protected $assigner_role_id;
  protected $assignee_role_id;

  /**
   * Contains data about all user roles, except anon.
   * @var UserRolesGrabber
   */
  protected $user_roles_grabber;
  
  public function __construct(
      $assigner_role_id,
      $assignee_role_id
    ) {
    $this->user_roles_grabber = UserRolesGrabber::getOneGrabber();
    $this->setAssignerRoleId($assigner_role_id);
    $this->setAssigneeRoleId($assignee_role_id);
  }

  /**
   * Set assigner role id.
   * @param int $role_id Role id.
   */
  public function setAssignerRoleId($role_id) {
    $this->set_role_id($role_id, 'assigner');
  }
  
  /**
   * Set assignee role id.
   * @param int $role_id Role id.
   */
  public function setAssigneeRoleId($role_id) {
    $this->set_role_id($role_id, 'assignee');
  }

  /**
   * Set role id.
   * @param int $role_id Role id.
   * @param string $which Which to set - assigner or assignee
   * @throws Exception Bad thing!
   */
  protected function set_role_id($role_id, $which) {
    if ( ! is_int($role_id) ) {
      throw new Exception(
          'AssignerAssigneeRoleMapping: set_role_id: role id not an integer: ' . 
          $role_id . ' Check with your dog.'
      );
    }
    if ( ! $this->user_roles_grabber->doesRoleIdExist($role_id) ) {
      throw new Exception(
          'AssignerAssigneeRoleMapping: set_role_id: role id does not exist: ' . 
          $role_id . ' Check with your dog.'
      );
    }
    if ( $which == 'assigner' ) {
      $this->assigner_role_id = $role_id;
    }
    else if ( $which == 'assignee' ) {
      $this->assignee_role_id = $role_id;
    }
    else {
      throw new Exception(
          'AssignerAssigneeRoleMapping: set_role_id: bad which: ' . 
          $which . ' Check with your dog.'
      );
    }
  }
  
  /**
   * Get assigner role id.
   * @return int role id.
   */
  public function getAssignerRoleId() {
    return $this->assigner_role_id;
  }  

  /**
   * Get assignee role id.
   * @return int role id.
   */
  public function getAssigneeRoleId() {
    return $this->assignee_role_id;
  }  

  /**
   * Get assigner role's name.
   * @return string Role name.
   */
  public function getAssignerRoleName() {
    return $this->user_roles_grabber->getRoleName($this->assigner_role_id);
  }
  
  /**
   * Get assignee role's name.
   * @return string Role name.
   */
  public function getAssigneeRoleName() {
    return $this->user_roles_grabber->getRoleName($this->assignee_role_id);
  }

}

/**
 * A collection of mappings. Iterable. (Is that really a word?)
 */
class AssignerAssigneeRoleMappingCollection implements Iterator {

  //Drupal persistent varaible the data is being stored in.
  const storageVariableName = 'domain_reference_roles_assign_mappings';
  
  //Array of RoleRoleMapping objects.
  protected $role_role_mappings = array();
  
  //Contains data about all user roles, except anon.
  protected $user_roles_grabber = NULL;  
  
  //Object storing metadata about domain_reference field instances.
  protected $instance_metadata_grabber = NULL;

  //Collection of field->role mapping objects managed by the domain_reference_roles module.
  protected $field_role_mappings = NULL;
  
  //All roles. Array. Index is role id, value is role name.
//  protected $all_roles = array();
  
  //Entity type the mapping is for.
//  public $entity_type;
  //Bundle the mapping is for.
//  public $bundle;

  //Drupal variable name for persistent storage. 
//  public $drupal_storage_var_name = NULL;
  
  //Flag set for testing. Loads fake data.
  public static $testing = FALSE;
  
  //Simulates responses to Drupal calls.
  public static $testing_data;
  
  //Simulates persistent storage for testing.
  public static $testing_persistent_storage;
  
  /**
   * Load mapping data from Drupal variable, prepare collection of mapping objects.
   */
  public function __construct(
//        $entity_type = 'user', 
//        $bundle = 'user', 
//        $drupal_storage_var_name = DOMAIN_REFERENCE_ROLES_ASSIGN_VAR_NAME
      ) {
    //Get metadata about field instances.
    $this->instance_metadata_grabber = 
        InstanceMetadataGrabber::getOneGrabber('domain_reference');
    //Get existing roles - drop anon.
    $this->user_roles_grabber = UserRolesGrabber::getOneGrabber();
//    if ( ! $drupal_storage_var_name ) {
//      throw new Exception(
//          'RoleRoleMappingCollection.constructor: ' 
//          . t('Storage variable name missing.')
//      );
//    }
//    $this->drupal_storage_var_name = $drupal_storage_var_name;
//    $this->entity_type = $entity_type;
//    $this->bundle = $bundle;
    //Get the domain reference fields -> role mappings.
    $this->field_role_mappings = new FieldRoleMappingCollection();
//    //Get existing roles - drop anon.
//    $this->all_roles = ( ! AssignerAssigneeRoleMappingCollection::$testing ) 
//        ? user_roles(TRUE) 
//        : AssignerAssigneeRoleMappingCollection::$testing_data['user_roles'];;
    $this->loadMappings();
  } // End constructor

  /**
   * Load mappings from storage. 
   * $this->domain_reference_fields and $this->roles have already been loaded.
   */
  protected function loadMappings() {
    $stored_mappings = ( ! AssignerAssigneeRoleMappingCollection::$testing ) ? 
      variable_get( AssignerAssigneeRoleMappingCollection::storageVariableName, array() ) 
      : AssignerAssigneeRoleMappingCollection::$testing_data['initial_mappings_storage'];
    if ( sizeof($stored_mappings) > 0 ) {
      foreach ( $stored_mappings as $role_role_mapping ) { 
        if (   ! isset($role_role_mapping['assigner_role_id']) 
            || ! isset($role_role_mapping['assignee_role_id']) ) {
          throw new Exception(
              'RoleRoleMappingCollection.loadMappings: ' . t('Bad mapping')
              . ': ' . print_r($role_role_mapping, TRUE)
          );
        }
        $assigner_role_id = $role_role_mapping['assigner_role_id'];
        $assignee_role_id = $role_role_mapping['assignee_role_id'];
        //Ensure the roles have domain reference fields associated. 
        //Don't throw exception - roles might have been deleted.
        $assigner_role_id_exists = $this->user_roles_grabber->
            doesRoleIdExist($assigner_role_id);
        $assignee_role_id_exists = $this->user_roles_grabber->
            doesRoleIdExist($assignee_role_id);
        if ( $assigner_role_id_exists && $assignee_role_id_exists ) {
          //Add new mapping.
          $this->role_role_mappings[] = new AssignerAssigneeRoleMapping(
              $assigner_role_id,
              $assignee_role_id
          );
        }
      }//End foreach mapping.
    } //End if there are stored mappings.
  }
  
  /**
   * Store the mappings in a Drupal variable.
   */
  public function storeMappings() {
    //Build a storage array.
    $storage_array = array();
    foreach ( $this->role_role_mappings as $role_role_mapping ) {
//      if ( !isset($role_role_mapping->assigner_role_id) || !isset($role_role_mapping->assignee_role_id) ) {
//        throw new Exception(
//            'RoleRoleMappingCollection.storeMapping: ' . t('Bad mapping') . ': ' 
//                . print_r($role_role_mapping, TRUE)
//        );
//      }
      $storage_array[] = array(
        'assigner_role_id' => $role_role_mapping->getAssignerRoleId(),
        'assignee_role_id' => $role_role_mapping->getAssigneeRoleId()
      );
    }
    if ( ! AssignerAssigneeRoleMappingCollection::$testing ) {
      variable_set( AssignerAssigneeRoleMappingCollection::storageVariableName, 
          $storage_array );
    }
    else {
      AssignerAssigneeRoleMappingCollection::$testing_persistent_storage = 
          $storage_array;
    }
  }
  
  /**
   * Check whether an assigner role exists and is mapped to a domain reference field.
   * @param type $assigner_role_id Role id to check.
   * @return boolean True if mapped, else false.
   */
  public function isAssignerRoleMappedToField($assigner_role_id) {
    return $this->is_role_mapped_to_field($assigner_role_id, 'assigner');
  }
  
  /**
   * Check whether an assignee role exists and is mapped to a domain reference field.
   * @param type $assignee_role_id Role id to check.
   * @return boolean True if mapped, else false.
   */
  public function isAssigneeRoleMappedToField($assignee_role_id) {
    return $this->is_role_mapped_to_field($assignee_role_id, 'assignee');
  }
  
  /**
   * Check whether a role exists and is mapped to a domain reference field.
   * @param int $role_id Role id to check.
   * @param string $which Role to check - assigner or assignee.
   * @return boolean True if mapped, else false.
   */
  protected function is_role_mapped_to_field($role_id, $which) {
    //Does the role exist in Drupal?
    if ( ! $this->user_roles_grabber->doesRoleIdExist($role_id) ) {
      return FALSE;
    }
    //Is the role in a mapping?
    foreach ( $this->field_role_mappings as $field_role_mapping ) {
      $mapping_role = ($which == 'assigner') 
          ? $field_role_mapping->getAssignerRoleId()
          : $field_role_mapping->getAssigneeRoleId();
      if ( $mapping_role == $role_id ) {
        return TRUE;
      }
    }
    return FALSE;
  }

  /**
   * Count the number of mappings.
   * @return integer Number of mappings.
   */
  public function countMappings() {
    return sizeof($this->role_role_mappings);
  }
  

//  /**
//   * Get roles that have been mapped to domain reference fields.
//   * @return array Roles. role_id => role_name.
//   */
//  public function getRolesMappedToFields() {
//    $mapped_roles = array();
//    foreach ( $this->field_role_mappings as $field_role_mapping ) {
//      $role_id = $field_role_mapping->getRoleId();
//      //Haven't saved it yet?
//      if ( ! array_key_exists($role_id, $mapped_roles) ) {
//        //Check that the role still exists.
//        if ( $this->user_roles_grabber->doesRoleIdExist($role_id) ) {
//          $mapped_roles[$role_id] = $field_role_mapping->getRoleName($role_id);
//        }
//      }
//    }
//    return $mapped_roles;
//  }

  /**
   * Get a mapping.
   * @param integer $assigner_role_id Role giving the assign.
   * @param integer $assignee_role_id Role receiving the assign.
   * @return Mapping, or null if not found.
   */
  public function getMapping($assigner_role_id, $assignee_role_id) {
    foreach ( $this->role_role_mappings as $mapping ) {
      if (    $mapping->getAssignerRoleId() == $assigner_role_id 
           && $mapping->getAssigneeRoleId() == $assignee_role_id ) {
        return $mapping;
      }
    }
    return NULL;
  }

  /**
   * Check whether a mapping already exists.
   * @param integer $assigner_role_id Role giving the assign.
   * @param integer $assignee_role_id Role receiving the assign.
   * @return boolean True if the mapping exists.
   */
  public function mappingExists($assigner_role_id, $assignee_role_id) {
    foreach ( $this->role_role_mappings as $mapping ) {
      if (    $mapping->getAssignerRoleId() == $assigner_role_id 
           && $mapping->getAssigneeRoleId() == $assignee_role_id ) {
        return TRUE;
      }
    }
    return FALSE;
  }
  
  /**
   * Add a new mapping to the collection.
   * @param integer $assigner_role_id Role giving the assign.
   * @param integer $assignee_role_id Role receiving the assign.
   * @throws Exception When mapping exists, or role ids are bad.
   */
  public function addMapping($assigner_role_id, $assignee_role_id) {
    if ( ! $this->user_roles_grabber->doesRoleIdExist($assigner_role_id) ) {
      throw new Exception(
          'RoleRoleMappingCollection.addMapping: ' . 
          t('Assigner role does not exist. Check with your dog.') . 
          " Assigner id: $assigner_role_id  Assignee id: $assignee_role_id"
      );
    }
    if ( ! $this->user_roles_grabber->doesRoleIdExist($assignee_role_id) ) {
      throw new Exception(
          'RoleRoleMappingCollection.addMapping: ' . 
          t('Assignee role does not exist. Check with your dog.') . 
          " Assigner id: $assigner_role_id  Assignee id: $assignee_role_id"
      );
    }
    if ( $this->mappingExists($assigner_role_id, $assignee_role_id) ) {
      throw new Exception(
          'RoleRoleMappingCollection.addMapping: ' . 
          t('Trying to add a mapping that already exists. Sad dog.') . 
          " Assigner id: $assigner_role_id  Assignee id: $assignee_role_id"
      );
    }
    $mapping = new AssignerAssigneeRoleMapping(
      $assigner_role_id,
      $assignee_role_id
    );
    $this->role_role_mappings[] = $mapping;
  }
  
  /**
   * Remove a mapping from the collection.
   * @param integer $assigner_role_id Role giving the assign.
   * @param integer $assignee_role_id Role receiving the assign.
   * @throws Exception When mapping not found.
   */
  public function eraseMapping($assigner_role_id, $assignee_role_id) {
    if ( ! $this->mappingExists($assigner_role_id, $assignee_role_id) ) {
      throw new Exception(
          'RoleRoleMappingCollection.eraseMapping: ' . 
          t('Trying to erase a nonexistent mapping. Sad dog.') . 
          " Assigner id: $assigner_role_id  Assignee id: $assignee_role_id"
      );
    }
    foreach ( $this->role_role_mappings as $key => $mapping ) {
      if (    $mapping->getAssignerRoleId() == $assigner_role_id 
           && $mapping->getAssigneeRoleId() == $assignee_role_id ) {
        unset($this->role_role_mappings[$key]);
        return;
      }
    } //end foreach
    throw new Exception(
        'RoleRoleMappingCollection.eraseMapping: ' . 
          t('Got to end of loop! Get another dog!') . 
          " Assigner id: $assigner_role_id  Assignee id: $assignee_role_id"
    );
  }

  /**
   * Erase the existing mapppings.
   */
  public function eraseAllMappings() {
    $this->role_role_mappings = array();
    $this->storeMappings();
  }

  //Implement methods of iterator interface.
  //See http://php.net/manual/en/language.oop5.iterations.php.
  public function rewind() {
    reset($this->role_role_mappings);
  }
  public function current() {
    return current($this->role_role_mappings);
  }
  public function key() {
    return key($this->role_role_mappings);
  }
  public function next() {
    return next($this->role_role_mappings);
  }
  public function valid() {
    $key = key($this->role_role_mappings);
    return ($key !== NULL && $key !== FALSE);
  }

}

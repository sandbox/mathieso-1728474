<?php
/**
 * @file
 * Blocks defined for the current site. Iterable. (There's that strange word again.)
 */
class SitewideBlocks implements Iterator {
  protected $blocks = array();
  
  public function __construct() {
    module_load_include('inc', 'block', 'block.admin');
    //All blocks exist for all themes. 
    global $theme;
    $blocks = block_admin_display_prepare_blocks($theme);
    //Sort array by block display name.
    $succeed_flag = usort($blocks, array('SitewideBlocks', 'compareBlocks'));
    if ( ! $succeed_flag ) {
      throw Exception('SitewideBlocks: __construct: Sorting failed. $blocks: '
          . '<pre>' . print_r($blocks, TRUE) . '</pre>');
    }
    //Add useful keys to array elements.
    foreach ( $blocks as $block ) {
      $key = $this->makeKey($block['module'], $block['delta']);
      $this->blocks[$key] = $block;
    }
  }
  
  /**
   * Comparison callback for usort(). Sort by block display name.
   * @param type $b1 A block.
   * @param type $b2 Another block.
   * @return int Comparison result.
   */
  private static function compareBlocks($b1, $b2) {
    if ( $b1['info'] < $b2['info'] ) {
      return -1;
    }
    if ( $b1['info'] > $b2['info'] ) {
      return 1;
    }
    return 0;
  }
  
  /**
   * Make a key from block info.
   * @param type $block_module Module creating the block.
   * @param type $block_delta Delta given by the module.
   * @return type Key.
   */
  protected function makeKey($block_module, $block_delta) {
    return $block_module . '|' . $block_delta;
  }

  /**
   * Check whether parameters define existing block.
   * @param string $block_module Value to check.
   * @param string $block_delta Value to check.
   * @return boolean True if parameter is a block id.
   */
  public function blockExists($block_module, $block_delta) {
    $key = $this->makeKey($block_module, $block_delta);    
    return array_key_exists($key, $this->blocks);
  }
  
  /**
   * Fetch a block.
   * @param string $block_module Value to check.
   * @param string $block_delta Value to check.
   * @return array Block data, NULL if block not found.
   */
  public function getBlock($block_module, $block_delta) {
    $key = $this->makeKey($block_module, $block_delta);    
    return array_key_exists($key, $this->blocks) ? $this->blocks[$key] : NULL;
  }

  /**
   * Get the display name of a block.
   * @param string $block_module Id the block.
   * @param string $block_delta Id the block.
   * @return string Display name, or NULL if not found.
   */
  public function getBlockDisplayName($block_module, $block_delta) {
    $key = $this->makeKey($block_module, $block_delta);    
    return array_key_exists($key, $this->blocks) ? $this->blocks[$key]['info'] : NULL;
  }

  /**
   * Remove a block from the collection.
   * @param type $block_module Block id part.
   * @param type $block_delta Block id part.
   */
  public function removeBlock($block_module, $block_delta) {
    $key = $this->makeKey($block_module, $block_delta);
    unset( $this->blocks[$key] );
  }
  
  //Implement methods of iterator interface.
  //See http://php.net/manual/en/language.oop5.iterations.php.
  public function rewind() {
    reset($this->blocks);
  }
  public function current() {
    return current($this->blocks);
  }
  //Iterator returns display name as key. Not the same as the internal key.
  public function key() {
    $block = current($this->blocks);
    return $block['info'];
  }
  public function next() {
    return next($this->blocks);
  }
  public function valid() {
    $key = key($this->blocks);
    return ($key !== NULL && $key !== FALSE);
  }
}


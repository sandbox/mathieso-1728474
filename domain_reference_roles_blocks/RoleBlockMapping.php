<?php
/**
 * @file
 * Classes encapsulating mappings betwixt roles managed by the domain 
 * roles module, and blocks.
 */

/**
 * One mapping.
 */
class RoleBlockMapping {
  public $role_id;
  public $block_module;
  public $block_delta;
  
  public function __construct(
      $role_id,
      $block_module,
      $block_delta
    ) {
    $this->role_id = $role_id;
    $this->block_module = $block_module;
    $this->block_delta = $block_delta;
  }
}

/**
 * A collection of mappings. Iterable. (Is that really a word?)
 */
class RoleBlockMappingCollection implements Iterator {
  //Drupal variable used to store mappings.
  const storageVariableName = 'domain_reference_roles_blocks_mappings';

  //Array of RoleBlockMapping objects.
  protected $mappings = array();
  //Roles and blocks sitewide.
  protected $sitewide_blocks = NULL;
  protected $sitewide_roles = NULL;
  
  /**
   * Load mapping data from Drupal variable, prepare collection of mapping objects.
   * @param SitewideBlocks $sitewide_blocks All blocks for the site.
   */
  public function __construct(SitewideBlocks $sitewide_blocks) {
    //Keep references to sitewide info.
    $this->sitewide_roles = user_roles(TRUE); //Excludes anon.
    $this->sitewide_blocks = $sitewide_blocks;
    $stored_records = variable_get( 
        RoleBlockMappingCollection::storageVariableName, 
        array()
    );
    if ( sizeof($stored_records) > 0 ) {
      foreach ( $stored_records as $record ) { 
        $role_id = $record->role_id;
        $block_module = $record->block_module;
        $block_delta = $record->block_delta;
        //Ensure role and block exist in the site.
        $role_exists = array_key_exists($role_id, $this->sitewide_roles);
        $block_exists = $sitewide_blocks->blockExists($block_module, $block_delta);
        if ( $block_exists && $role_exists ) {
          //Add new mapping.
          $this->mappings[] = new RoleBlockMapping(
              $role_id,
              $block_module,
              $block_delta
          );
        }
      }
    }
  }// End constructor
  
  /**
   * Mapping exists?.
   * @param integer $role_id Mapping's role id.
   * @param string $block_module Mapping's block id part.
   * @param string $block_delta Mapping's block id part.
   * @return boolean True if mapping is in the collection.
   */
  public function mappingExists($role_id, $block_module, $block_delta) {
    foreach ( $this->mappings as $mapping ) {
      if (    $mapping->role_id == $role_id 
           && $mapping->block_module == $block_module
           && $mapping->block_delta == $block_delta ) {
        return TRUE;
      }
    }
    return FALSE;
  }

  
  /**
   * Add a new mapping to the collection.
   * @param integer $role_id Role id
   * @param string $block_module Block id part.
   * @param string $block_delta Block id part.
   * @throws Exception When role or block info are bad.
   */
  public function addMapping($role_id, $block_module, $block_delta) {
    if ( ! array_key_exists( $role_id, $this->sitewide_roles) ) {
      throw new Exception(
          'RoleBlockMapping.addMapping: ' . t('Role does not exist') .
          " Role id: $role_id  Block module: $block_module  Block delta: $block_delta"
      );
    }
    if ( ! $this->sitewide_blocks->blockExists($block_module, $block_delta) ) {
      throw new Exception(
          'RoleBlockMapping.addMapping: ' . t('Block does not exist') .
          " Role id: $role_id  Block module: $block_module  Block delta: $block_delta"
      );
    }
    if ( ! $this->mappingExists($role_id, $block_module, $block_delta) ) {
      $mapping = new RoleBlockMapping(
          $role_id,
          $block_module,
          $block_delta
      );
      $this->mappings[] = $mapping;
    }
  }
  
  /**
   * Store the mappings in a Drupal variable.
   */
  public function storeMappings() {
    variable_set( 
        RoleBlockMappingCollection::storageVariableName, 
        $this->mappings 
    );
  }
  
  /**
   * Erase the existing mappings.
   */
  public function eraseAllMappings() {
    $this->mappings = array();
    variable_set( 
        RoleBlockMappingCollection::storageVariableName,
        array()
    );
  }

  //Implement methods of iterator interface.
  //See http://php.net/manual/en/language.oop5.iterations.php.
  public function rewind() {
    reset($this->mappings);
  }
  public function current() {
    return current($this->mappings);
  }
  public function key() {
    return key($this->mappings);
  }
  public function next() {
    return next($this->mappings);
  }
  public function valid() {
    $key = key($this->mappings);
    return ($key !== NULL && $key !== FALSE);
  }

}

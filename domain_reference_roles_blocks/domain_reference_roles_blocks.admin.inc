<?php
/**
 * @file
 * Defines an admin form for the domain reference roles blocks module.
 * Admin users select the blocks that users see on the domains that domain
 * reference fields give them access to.
 */

module_load_include('inc', 'domain_reference', 'InstanceMetadataGrabber');
module_load_include('inc', 'domain_reference_roles', 'UserRolesGrabber');
module_load_include('inc', 'domain_reference_roles', 'FieldRoleMapping');
module_load_include('inc', 'domain_reference_roles_assign', 'AssignorAssigneeRoleMapping');

/**
 * Separator used to build unique identifiers for checkboxes.
 * OK, it looks hacky, but it works.
 */
define('_DR_CHECKBOX_IDENTIFICATION_SEPARATOR', '|dog|');

/**
 * Define the role/block mapping form.
 */
function _drb_role_block_form($form, &$form_state) {
  try {
    $form['#submit'] = array('_drb_role_block_form_submit');
    $form['instructions'] = array(
      '#markup' => '<p>' . t('Select blocks whose visibility is determined by 
        roles granted by domain reference fields.') .
        '<p>' . t('Your selections apply to all themes.') . '</p>',
    );
    //Get metadata for all the instances of domain_reference field.
    $instance_metadata_grabber = InstanceMetadataGrabber::getOneGrabber('domain_reference');
    //Exit if there are no domain reference fields.
    if ( $instance_metadata_grabber->countInstances() == 0 ) {
      $form['error'] = array(
        '#markup' => '<p>' . t('There are no domain reference fields for users.') . '</p>
                      <p>See ' . l( t('help'), 'admin/help#domain_reference') . ' for information.</p>',
      );
      return $form;
    }
    //Load the current field->role mappings from the user object.
    $field_role_mappings = new FieldRoleMappingCollection();
    //Load all blocks for this site.
    $sitewide_blocks = new SitewideBlocks();
    //Load the current role/block mappings.
    $role_block_mappings = new RoleBlockMappingCollection($sitewide_blocks);;
    //Get the roles referenced in the field->role mappings.
    $domain_reference_roles = array();
    foreach ( $field_role_mappings as $field_role_mapping ) {
      if ( ! array_key_exists($field_role_mapping->getRoleId(), $domain_reference_roles) ) {
        $domain_reference_roles[$field_role_mapping->getRoleId()] = 
            $field_role_mapping->getRoleName();
      }
    }
    //Make a headers array.
    $header = array('Block');
    foreach ( $domain_reference_roles as $role_name) {
      $header[] = check_plain($role_name);
    }
    $form['header'] = array(
      '#type' => 'value',
      '#value' => serialize($header),
    );

    //Make a 2D array of checkboxes - roles as columns, blocks as rows.
    //Include all combinations of roles and blocks.
    $form['block table'] = array();
    foreach ( $sitewide_blocks as $block_display_name => $block ) {
      $row = array();
      $row[] = array(
        '#type' => 'markup',
        '#markup' => check_plain($block_display_name),
      );
      foreach ($domain_reference_roles as $role_id => $role_name) {
        //Has the role/block mapping been set?
        $checked = $role_block_mappings->mappingExists(
            $role_id,
            $block['module'],
            $block['delta']
        );
        //Create a unique identifier for the checkbox.
        $checkbox_identifier = 
            $role_id . _DR_CHECKBOX_IDENTIFICATION_SEPARATOR .
            $block['module'] . _DR_CHECKBOX_IDENTIFICATION_SEPARATOR .
            $block['delta'];
        $row[$checkbox_identifier] = array(
          '#type' => 'checkbox',
          '#default_value' => $checked,
          //Value returned when box is checked.
          '#return_value' => $checkbox_identifier,
          '#attributes' => array(
            'title' => array($role_name . '/' . $block_display_name),
          ),
        );
      } //End foreach role.
      $form['block table'][$block_display_name] = $row;
    } // end foreach block.
    $form['add'] = array(
      '#type' => 'submit',
      '#value' => t('Save'),
    );
    return $form;
  } catch (Exception $e) {
    domain_reference_roles_log_exception('Domain reference roles blocks', $e);
  }
}

/**
 * Handle role/block mapping form submission.
 */
function _drb_role_block_form_submit($form, &$form_state) {
  try {
    //Load all blocks for this site.
    $sitewide_blocks = new SitewideBlocks();
    //Load the current role/block mappings.
    $role_block_mappings = new RoleBlockMappingCollection($sitewide_blocks);;
    $role_block_mappings->eraseAllMappings();
    foreach ( $form_state['values'] as $value ) {
      if ( strstr($value, _DR_CHECKBOX_IDENTIFICATION_SEPARATOR) !== FALSE ) {
        $values = explode(_DR_CHECKBOX_IDENTIFICATION_SEPARATOR, $value);
        $role_id = $values[0];
        $block_module = $values[1];
        $block_delta = $values[2];
        $role_block_mappings->addMapping($role_id, $block_module, $block_delta);
      }
    }
    $role_block_mappings->storeMappings();
    drupal_set_message( t('Block-role associations saved.') );
  } catch (Exception $e) {
    domain_reference_roles_log_exception('Domain reference roles blocks', $e);
  }
}

/**
 * Returns HTML for the roles/block admin form.
 */
function theme__drb_role_block_form($variables) {
  try {
    $form = $variables['form'];
    if (!empty($form['error'])) {
      $output = drupal_render($form['error']);
      return $output;
    }
    $output = '';
    $output .= drupal_render($form['instructions']);
    $header = unserialize($form['header']['#value']);
    //unset($form['header']);
    $rows = array();
    foreach ( element_children($form['block table']) as $key ) {
      $row_output = array();
      $row_elements = element_children($form['block table'][$key]);
      foreach ( $row_elements as $item ) {
        $rendered = drupal_render($form['block table'][$key][$item]);
        $row_output[] = array(
          'data' => $rendered,
          'class' => array($item),
        );
      }
      $rows[] = array(
        'data' => $row_output,
        'class' => array('domain_reference_roles_role_block_mapping'),
      );
    }
    $attributes = array(
      'id' => 'domain_reference_roles_role_block_mappings',
    );
    $output .= theme('table', array(
        'header' => $header, 
        'rows' => $rows, 
        'attributes' => $attributes,
    ));
    $output .= drupal_render_children($form);
    return $output;
  } catch (Exception $e) {
    domain_reference_roles_log_exception('Domain reference roles blocks', $e);
  }
}

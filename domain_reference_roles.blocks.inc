<?php

/**
 * Implements hook_block_info().
 * 
 * Defines a block showing user roles on the current domain.
 * @return array Block definition.
 */
function domain_reference_roles_block_info() {
  $blocks = array();
  $blocks['user_roles'] = array(
    'info' => 'User roles',
    'cache' => DRUPAL_NO_CACHE,
  );
  return $blocks;  
}

/**
 * Implements hook_block_view().
 */
function domain_reference_roles_block_view($delta = '') {
  $block = array();
  switch ($delta) {
    case 'user_roles' :
      $block['subject'] = t('User roles');
      $block['content'] = _domain_reference_roles_roles_list();
      return $block;
      break;
  }
}

function _domain_reference_roles_roles_list() {
  $output = '';
  $field_role_mappings = new FieldRoleMappingCollection();
  global $user;
  $user_name = $user->uid == 0 ? '(Anonymous)' : $user->name;
  $output .= 
      '<p class=\'user-name-container\'>User: ' .
        '<span class=\'user-name\'>' . $user_name . '</span>' .
      '</p>';
  if ( sizeof($user->roles) == 0 ) {
    //KRM: should this ever happen? Dog knows.
    $output .= '<p class=\'title-no-normal-roles\'>' . t('No normal roles') . '</p>';    
  }
  else {
    $output .= '<p class=\'title-normal-roles\'>' . t('Normal roles') . '</p>';
    $items = array();
    foreach ( $user->roles as $role_id => $role_name ) {
      $items[] = $role_name;
    }
    $output .= theme(
        'item_list', 
        array(
            'type' => 'ul',
            'items' => $items
        )
    );
  }
  $extra_roles = domain_reference_roles_get_extra_roles(
      $user, 
      $field_role_mappings
  );
  if ( sizeof($extra_roles) == 0 ) {
    $output .= '<p class=\'title-no-extra-roles\'>' . t('No extra roles') . '</p>';    
  }
  else {
    $items = array();
    foreach ( $extra_roles as $role_id => $role_name ) {
      $items[] = $role_name;
    }
    $output .= '<p class=\'title-extra-roles\'>' . t('Extra roles') . '</p>';
    $output .= theme(
        'item_list', 
        array(
            'type' => 'ul',
            'items' => $items
        )
    );
  }
  return $output;
}
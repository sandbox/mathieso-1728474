<?php

/**
 * @file
 * Plugin to provide access control based upon active domain.
 */

module_load_include('inc', 'domain_reference', 'InstanceMetadataGrabber');
module_load_include('inc', 'domain_reference_roles', 'UserRolesGrabber');
module_load_include('inc', 'domain_reference_roles', 'FieldRoleMapping');
module_load_include('module', 'domain_reference_roles', 'domain_reference_roles');

/**
 * Define the plugin.
 */
$plugin = array(
    'title' => t('Domain reference role access'),
    'description' => t('Control access by domain reference fields.'),
    'callback' => '_drrc_check',
    'default' => array('roles' => array()),
    'settings form' => '_drrc_settings',
    'settings form submit' => '_drrc_settings_submit',
    'summary' => '_drrc_summary',
//    'required context' => new ctools_context_required(t('User'), 'user'),
  );


/**
 * Settings form for domain reference roles plugin.
 */
function _drrc_settings($form, &$form_state, $conf) {
  //Get the field-role mappings.
  $mappings = new FieldRoleMappingCollection();
  //Get the roles involved.
  $roles_available = $mappings->getRolesMappedToFields();
  //Exit if there are no domain reference fields.
  if ( sizeof( $roles_available ) == 0 ) {
    $form['error'] = array(
      '#markup' => '<p>' . t('Sorry, there are no roles assigned by domain reference fields for users.') . '</p>
                    <p>See ' . l( t('help'), 'admin/help#domain_reference') . ' for information.</p>',
    );
    return $form;
  }
  $form['settings']['roles'] = array(
    '#type' => 'checkboxes',
    '#title' => 'Roles assigned by domain reference fields',
    '#description' => 'Please select the fields that will determine access.',
    '#options' => $roles_available,
    '#default_value' => array_keys( $conf['roles'] ),
  );
  return $form;
}

/**
 * Submit settings form for domain reference roles plugin.
 */
function _drrc_settings_submit($form, &$form_state) {
  $user_roles_grabber = UserRolesGrabber::getOneGrabber();
  $new_values = array();
  foreach ( $form_state['values']['settings']['roles'] as $role_id => $checked ) {
    if ( $checked != 0 ) {
      $new_values[$role_id] = $user_roles_grabber->getRoleName($role_id);
    }
  }
  $form_state['values']['settings']['roles'] = $new_values;
}



/**
 * Check for access based on the currently active domain.
 */
function _drrc_check($conf, $context, $plugin) {
  global $user;
  $mappings = new FieldRoleMappingCollection();
  //If no mappings, no access.
  if ( $mappings->countMappings() == 0 ) {
    return FALSE;
  }
  $extra_roles = domain_reference_roles_get_extra_roles($user, $mappings);
  $common_roles = array_intersect($extra_roles, $conf['roles']);
  return ( sizeof($common_roles) > 0 );
}

/**
 * Provide a summary description based upon the checked domains.
 */
function _drrc_summary($conf, $context) {
  $output = 'Roles for access: ';
  if ( sizeof( $conf['roles'] ) == 0 ) {
    $output .= 'None';
  }
  else {
    $prefix = '';
    foreach ( $conf['roles'] as $role_name ) {
      $output .= $prefix . $role_name;
      $prefix = ', ';
    }
  }
  return $output;
}

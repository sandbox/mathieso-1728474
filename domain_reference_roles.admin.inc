<?php
/**
 * @file
 * Defines an admin form for the domain reference roles  module.
 * Admin users select the roles that users have on the domains listed in domain
 * reference fields.
 */

/**
 * Build form for associating domain reference fields and roles.
 */
function _dr_field_role_form() {
  try {
    //Get metadata for all the instances of domain_reference field.
    $instance_metadata_grabber = InstanceMetadataGrabber::getOneGrabber('domain_reference');
    $instances_metadata = $instance_metadata_grabber->getAllMetadata();
    //Load roles
    $roles_grabber = UserRolesGrabber::getOneGrabber();
    $roles = $roles_grabber->getAllRoles();
    $form = array();
    $form['#validate'] = array('_dr_field_role_form_validate');
    $form['#submit'] = array('_dr_field_role_form_submit');
    $form['instructions'] = array(
      '#markup' => '<p>' . t('Choose domain reference fields ' .
        'that will add corresponding roles on domains.') . '</p>',
    );
    //Load the current field->role mappings
    $mappings = new FieldRoleMappingCollection();
    //Exit if there are no domain reference fields.
    if ( $instance_metadata_grabber->countInstances() == 0 ) {
      $form['error'] = array(
        '#markup' => '<p>' . t('There are no domain reference fields for users.') . '</p>
                      <p>See ' . l( t('help'), 'admin/help#domain_reference') . ' for information.</p>',
      );
      $mappings->eraseAllMappings();
      return $form;
    }
    //Make mappings ready to show.
    if ( $mappings->countMappings() == 0 ) {
      $message = '(Currently, there are no associations between fields and roles.
                  You can add one below.)';
      $form['no_mappings'] = array(
        '#markup' => '<p>' . t($message) . '</p>',
      );
    }
    else {
      //Show mappings table.
      $form['current_instructions'] = array(
        '#markup' => '<p>' . t('Current associations:') . '</p>',
      );
      $header = array(
        t('Field'), 
        t('Role'),
        t('Operations'),
      );
      $rows = array();
      //$instance_metadata is sorted in the right order for presentation.
      //Run through that, and look for corresponding elements in the 
      //current mapping. When find one, output it.
      foreach ( $instances_metadata as $instance_metadata ) {
        $instance_id = $instance_metadata['instance_id'];
        $mapping = $mappings->getMappingForInstance($instance_id);
        if ( ! is_null($mapping) ) {
          //Build the output row.
          $row = array();
          $instance_text = t($instance_metadata['instance_label']) .
              ' (' . t($instance_metadata['bundle_label']) . ')<br/>' .
              t(' Entity: ') . $instance_metadata['entity_name'] . 
              t(', bundle: ') . $instance_metadata['bundle_name'] .
              t(', field: ') . $instance_metadata['field_name'];
          $row['instance'] = $instance_text;
          $row['role_name'] = check_plain($mapping->getRoleName());
          $row['delete'] = l(
              t('Delete'), 
              'admin/structure/domain/domain-roles/delete/' . $mapping->getInstanceId()
                  . '/' . $mapping->getRoleId()
          );
          $rows[] = $row;
          //Add to the dropdown option list for instances.
        }
      }
      $form['mappings'] = array(
        '#theme' => 'table',
        '#rows' => $rows,
        '#header' => $header,
      );        
    } // end mapping count test.
    //Create instance options list.
    $instance_options = array();
    foreach ( $instances_metadata as $instance_metadata ) {
      $instance_id = $instance_metadata['instance_id'];
      $instance_options[$instance_id] = t($instance_metadata['instance_label']) .
            ' (' . t($instance_metadata['bundle_label']) . ')';
    }    
    //Controls for list of fields and roles, for adding new mapping.
    $form['add_new'] = array(
      '#markup' => '<p>' . t('You can add a new association.') . '</p>',
    );
    //Fields
    $form['new_field_instance'] = array(
      '#type' => 'select',
      '#title' => t('Domain reference field'),
      '#description' => t('Domain reference field associated with a role'),
      '#options' => $instance_options,
    );
    //Roles
    $form['new_role'] = array(
      '#type' => 'select',
      '#title' => t('Role'),
      '#description' => t('Role associated with seleted domain reference field'),
      '#options' => $roles,
    );
    //Button to add new mapping.
    $form['add'] = array(
      '#type' => 'submit',
      '#value' => t('Add role'),
    );
    return $form;
  } catch (Exception $e) {
    domain_reference_roles_blocks_log_exception( $e );
  }
}

/**
 * Validate form submission.
 */
function _dr_field_role_form_validate($form, &$form_state) {
  try {
    $new_instance_id = (int) $form_state['values']['new_field_instance'];
    $new_role_id = (int) $form_state['values']['new_role'];
    //Get metadata for all the instances of domain_reference field.
    $instance_metadata_grabber = InstanceMetadataGrabber::getOneGrabber('domain_reference');
    //Make sure field instance exists. If not, Big Trouble in Domain reference roles.
    if ( ! $instance_metadata_grabber->doesInstanceIdExist($new_instance_id) ) {
      throw new Exception('domain_reference_roles_validate: unexpected field instance id: ' 
          . $new_instance_id);
    }
    //Load roles
    $roles_grabber = UserRolesGrabber::getOneGrabber();
    $roles = $roles_grabber->getAllRoles();
    //Make sure role exists. If not, Big Trouble in Domain reference roles.
    if ( ! isset( $roles[$new_role_id] ) ) {
      throw new Exception('domain_reference_roles_validate: unexpected role id: ' 
          . $new_role_id);
    }
    //Check whether this field/role combination is already in the list.
    //Get current mapping data.
    $current_mappings = new FieldRoleMappingCollection();
    if ( $current_mappings->mappingExists($new_instance_id, $new_role_id) ) {
      form_set_error('new_field_instance', 
          'There is already an association between that field and role.');
      return;
    }
    //Warn if role is administrator.
    $admin_roles = user_roles(TRUE, 'administer modules');
    if ( array_key_exists($new_role_id, $admin_roles) ) {
      drupal_set_message( 
          t('Warning: You have granted access to an administrator role.'), 
          'warning'
      );
    }
  } catch (Exception $e) {
    domain_reference_roles_blocks_log_exception( $e );
  }
}

/**
 * Handle field/role mapping form submission.
 */
function _dr_field_role_form_submit($form, &$form_state) {
  try {
    $new_instance_id = (int) $form_state['values']['new_field_instance'];
    $new_role_id = (int) $form_state['values']['new_role'];
    //Get mapping data.
    $mappings = new FieldRoleMappingCollection();
    //Store the new mapping.
    $mappings->addMapping($new_instance_id, $new_role_id);
    $mappings->storeMappings();
    drupal_set_message( t('New mapping added.') );
  } catch (Exception $e) {
    domain_reference_roles_blocks_log_exception( $e );
  }
}

/**
 * Returns HTML for the domain reference roles admin form.
 *
 * @param $variables
 *   An associative array containing:
 *   - form: A render element representing the form.
 *
 * @ingroup themeable
 */
//function theme__dr_field_role_form($variables) {
//  try {
//    $form = $variables['form'];
//    if (!empty($form['error'])) {
//      $output = drupal_render($form['error']);
//      return $output;
//    }
//    $output = '';
//    $output .= drupal_render($form['instructions']);
//    if ( isset($form['no_mappings']) ) {
//      $output .= drupal_render($form['no_mappings']);
//    }
//    if ( isset($form['mappings']) ) {
//      $header = array(
//        t('Domain reference field'), 
//        t('Role'),
//        t('Operation'),
//      );
//      $rows = array();
//      foreach ( element_children($form['mappings']) as $key ) {
//        $row_output = array();
//        $row_elements = element_children($form['mappings'][$key]);
//        foreach ( $row_elements as $item ) {
//          $rendered = drupal_render($form['mappings'][$key][$item]);
//          $row_output[] = array(
//            'data' => $rendered,
//            'class' => array($item),
//          );
//        }
//        $rows[] = array(
//          'data' => $row_output,
//          'class' => array('domain_reference_roles_field_role_mapping'),
//        );
//      }
//      $attributes = array(
//        'id' => 'domain_reference_roles_field_role_mappings',
//      );
//      $output .= theme('table', array(
//          'header' => $header, 
//          'rows' => $rows, 
//          'attributes' => $attributes,
//      ));
//    }
//    $output .= drupal_render_children($form);
//    return $output;
//  } catch (Exception $e) {
//    domain_reference_roles_log_exception( $e );
//  }
//}

/**
 * Form callback for delete confirmation form.
 * @param array $form Form definition.
 * @param type $field_instance_id Field instance id of mapping to delete.
 * @param type $role_id Role id of mapping to delete.
 * @return array Form renderable array.
 * @throws Exception If something strange and bad happens. Get another dog.
 */
function domain_reference_roles_confirm_delete_form($form, &$form_state, $field_instance_id, $role_id) {
  try {
    //Load mapping data.
    $mappings = new FieldRoleMappingCollection();
    //Check that the passed mapping exists.
    if ( ! $mappings->mappingExists($field_instance_id, $role_id) ) {
      throw new Exception("domain_reference_roles_confirm_delete_form: Attempt to delete 
          a mapping that does not exist. 
          Instance id: $field_instance_id  Role id: $role_id");
    } //End check passed mapping exists.
    //Lookup names of field instance and role.
    $mapping = $mappings->getMapping($field_instance_id, $role_id);
    //The field and role ids have already been checked. If they can't be found, 
    //something serious is wrong. Time for a dog alert.
    if ( is_null($mapping) ) {
      throw new Exception("domain_reference_roles_confirm_delete_form: something strange
        happened. Get another dog. field instance id: $field_instance_id  
        role id: $role_id");
    }
    //Return confirmation form.
    $question = 'Confirm domain role mapping deletion';
    $message = 
      '<p><strong>' . t('Are you sure you want to delete this mapping?') . '</strong></p>
       <ul>
         <li>' . t('Domain reference field: ') . 
            $mapping->getInstanceLabel() . ' (' . $mapping->getBundleLabel() . ')
         </li>
         <li>' . t('Role:') . ' ' . $mapping->getRoleName() . '</li>
       </ul>
       <p>' . t('This action cannot be undone.') . '</p>';
    return confirm_form($form, $question, 'admin/structure/domain/domain-roles', 
        $message, 'Delete', 'Cancel', 'delete_confirmation_item');
  } catch (Exception $e) {
    domain_reference_roles_blocks_log_exception( $e );
  }
}

/**
 * User confirmed delete.
 * This function checks for cases that should be impossible,
 * given all the validation that has occurred already. 
 * The security implications of this module make such paranoia appropriate.
 * @param array $form Form definition.
 * @param array $form_state Form state.
 */
function domain_reference_roles_confirm_delete_form_submit($form, &$form_state) {
  try {
    $confirm = $form_state['values']['delete_confirmation_item'];
    $instance_id = $form_state['build_info']['args'][0];
    $role_id = $form_state['build_info']['args'][1];
    //Load the mappings.
    $mappings = new FieldRoleMappingCollection();
    //$confirm should be 1. If not, something bad is happening.
    if ( $confirm != 1 ) {
      throw new Exception("Unexpected BAD THING. domain_reference_roles_confirm_delete_form_submit: 
          confirm is not 1. VERY STRANGE!
          Instance id: $instance_id  Role id: $role_id");
    }
    //Get the mapping to see if it exists.
    $mapping = $mappings->getMapping($instance_id, $role_id);
    if ( ! isset($mapping) ) {
      throw new Exception("Unexpected BAD THING. domain_reference_roles_confirm_delete_form_submit: 
        bad mapping. VERY STRANGE! Consult your dog.
        Instance id: $instance_id  Role id: $role_id");
    }
    //Check that the mapping exists.
    if ( ! $mappings->mappingExists($instance_id, $role_id) ) {
      throw new Exception("Unexpected BAD THING. domain_reference_roles_confirm_delete_form_submit: mapping does not exist. VERY STRANGE!
                  Instance id: $instance_id  Role id: $role_id");
    }
    $mappings->eraseMapping($instance_id, $role_id);
    $mappings->storeMappings();
    drupal_goto('admin/structure/domain/domain-roles');
  } catch (Exception $e) {
    domain_reference_roles_blocks_log_exception( $e );
  }
}


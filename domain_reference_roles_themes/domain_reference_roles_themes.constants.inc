<?php
/**
 * @file
 * Defines constants for the domain reference roles themes module.
 */

/**
 * Default path for the settings page.
 */
define('DOMAIN_REFERENCE_ROLES_THEMES_DEFAULT_PATH', 'manage-site/theme-settings');

/**
 * Default title for the settings page.
 */
define('DOMAIN_REFERENCE_ROLES_THEMES_DEFAULT_TITLE', 'Theme settings');

/**
 * Drupal variable storing the path for the theme setting page.
 */
define('DOMAIN_REFERENCE_ROLES_THEMES_PATH_VAR_NAME', 'DOMAIN_REFERENCE_ROLES_THEMES_PATH');

/**
 * Drupal variable storing the title for the theme setting page.
 */
define('DOMAIN_REFERENCE_ROLES_THEMES_TITLE_VAR_NAME', 'DOMAIN_REFERENCE_ROLES_THEMES_TITLE');

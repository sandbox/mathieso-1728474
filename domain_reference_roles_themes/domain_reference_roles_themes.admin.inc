<?php
/**
 * @file
 * Defines an administration form for the domain reference roles themes module. 
 * Lets an admin user set the path for the domain-specific theme settings form.
 */

module_load_include('inc', 'domain_reference_roles_themes', 
    'domain_reference_roles_themes.constants');

/**
 * Build the form for setting the path to the theme settings page.
 * @return array Renderable array with form.
 */
function _drt_set_path_form() {
  $current = _drt_get_current_settings_path();
  $form = array();
  $form['instructions'] = array(
    '#markup' => '<p>' . t('Enter the path to and title of the theme settings ' .
        'page. It will apply to all domains controlled by Domain role.') .
      '<p>' . t('Path example') . ':  ' . DOMAIN_REFERENCE_ROLES_THEMES_DEFAULT_PATH . '</p>
       <p>' . t('Current value: ') . l($current, $current) . '</p>'
  );
  $form['path'] = array(
    '#title' => 'Path',
    '#type' => 'textfield',
    '#description' => 'Enter the path.',
    '#default_value' => _drt_get_current_settings_path(),
    '#required' => TRUE,
  );
  $form['title'] = array(
    '#title' => 'Page title',
    '#type' => 'textfield',
    '#description' => 'Enter the page title.',
    '#default_value' => _drt_get_current_settings_title(),
    '#required' => FALSE,
  );
  $form['more_instructions'] = array(
    '#markup' => '<p>' . t('Remember to give a role permission to') . ' ' .
        l( t('change domain theme settings'), 
           url('admin/people/permissions',
               array('absolute' => TRUE, 'fragment' => 'module-domain_reference_roles_themes')
           )
        ) . 
        '. </p>'
  );
  //Can't use system_settings_form(), because Domain access module will alter it. 
  $form['save it'] = array(
    '#type' => 'submit',
    '#value' => 'Save configuration',
  );
  return $form;
}

/**
 * Implements hook_form_submit().
 */
function _drt_set_path_form_submit($form, &$form_state) {
  $current = _drt_get_current_settings_path();
  $new_path = $form_state['values']['path'];
  $new_title = $form_state['values']['title'];
  if ( $current != $new_path ) {
    if ( drupal_valid_path($new_path) ) {
      drupal_set_message( t('Warning: The path already exists in the current domain. 
          It may or may not exist in subdomains. If you still want access to the 
          page this path originally referred to, you should change the 
          path here. The value given in the example below will almost always work.'), 'warning');
    }
    variable_set(DOMAIN_REFERENCE_ROLES_THEMES_PATH_VAR_NAME, $new_path);
    variable_set(DOMAIN_REFERENCE_ROLES_THEMES_TITLE_VAR_NAME, $new_title);
    menu_rebuild();
  }
  drupal_set_message( t('Saved') );
  if ( $new_title == '' ) {
    drupal_set_message('The theme settings page will have no title.', 'warning');
  }
  
}
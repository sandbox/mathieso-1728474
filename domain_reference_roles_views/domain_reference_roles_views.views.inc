<?php
/**
 * A plugin to handle access control based on domain reference roles.
 */
class views_plugin_access_domain_reference_roles extends views_plugin_access {
  /**
   * Return a string to display as the clickable title for the
   * access control.
   */
  function summary_title() {
    $count = count($this->options['role']);
    if ($count < 1) {
      return t('No role(s) selected');
    }
    elseif ($count > 1) {
      return t('Multiple roles');
    }
    else {
      $mappings = new FieldRoleMappingCollection();
      $roles = $mappings->getRolesMappedToFields();
      //If no mappings, no access.
      if (sizeof($roles) == 0 ) {
        return t('No roles available');
      }
      $rid = reset($this->options['role']);
      return check_plain($roles[$rid]);
    }
  }

  /**
   * Determine if the logged in user has a role or not.
   */
  function access($account) {
    //array_filter removes items that evaluate to false.
    return _drrv_access(array_filter($this->options['role']), $account);
  }
  
  /**
   * Determine the access callback and arguments.                                                                     *
   */
  function get_access_callback() {
    //array_filter removes items that evaluate to false.
    return array('_drrv_access', array(array_filter($this->options['role'])));
  }

  function option_definition() {
    $options = parent::option_definition();
    $options['role'] = array('default' => array());
    return $options;
  }

  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);
    $mappings = new FieldRoleMappingCollection();
    $roles = $mappings->getRolesMappedToFields();
    //Check if no mappings.
    if (sizeof($roles) == 0 ) {
      $form['no_roles'] = array(
        '#markup' => t('No roles available'),
      );
    }
    else {
      $form['role'] = array(
        '#type' => 'checkboxes',
        '#title' => t('Role'),
        '#default_value' => $this->options['role'],
        '#options' => $roles,
        '#description' => t('Only the checked roles will be able to access ' .
            'this display. Note that only roles granted by domain reference ' .
            'fields are checked here. You may need other access checks as well.'),
      );
    }
  }

  function options_validate(&$form, &$form_state) {
    if (!array_filter($form_state['values']['access_options']['role'])) {
      form_error($form['role'], 
          t('Please select at least one role.'));
    }
  }

  function options_submit(&$form, &$form_state) {
    $form_state['values']['access_options']['role'] = 
      array_filter($form_state['values']['access_options']['role']);
  }

  
}
